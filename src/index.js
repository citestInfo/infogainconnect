import React from 'react';
import {render} from "react-dom";
import configstore from './configStore';
import Root from './components/Root';
import {BrowserRouter as Router} from 'react-router-dom';
import Firebase from './config/Firebase';
import FirebaseContext from './config/FirebaseContext';
// styles
import 'bootstrap/dist/css/bootstrap.css';

const store = configstore();
render(
  <FirebaseContext.Provider value = {new Firebase()}>
    <Router>
        <Root store = {store} />
    </Router>
  </FirebaseContext.Provider>,document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.

// serviceWorker.unregister();

