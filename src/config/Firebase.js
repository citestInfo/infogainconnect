import firebase from "firebase";
import { devConfig } from "./config";

if (!firebase.apps.length) {
    firebase.initializeApp(devConfig);
}
class Firebase {
    constructor() {
        this.auth = firebase.auth();
    }
    doCreateUserWithEmailAndPassword = (email, password) =>
        this.auth.createUserWithEmailAndPassword(email, password);
    doSignInWithEmailAndPassword = (email, password) =>
            this.auth.signInWithEmailAndPassword(email, password).then(res => window.location = "/dashboard")
          
        doSignOut = () => {
            localStorage.removeItem('userData')
            this.auth.signOut();
        }
        doPasswordReset = email => this.auth.sendPasswordResetEmail(email);
        doPasswordUpdate = password =>
        this.auth.currentUser.updatePassword(password);
}

export default Firebase;

// export const auth = firebase.auth();
const databaseRef = firebase.database().ref();

export const usersPostsRef = databaseRef.child("users/users");
export const dataRef = databaseRef.child("data/data");
export const latestRef = databaseRef.child("latest/latest");
export const jobsRef = databaseRef.child("jobs/jobs");
export const trainingRef = databaseRef.child("training/training");
export const hrPostsRef = databaseRef.child("hradmin/hradmin");
export const issPostsRef = databaseRef.child("issadmin/issadmin");
export const postsRef = databaseRef.child("posts/posts");
