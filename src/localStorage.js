export const loadState=()=>{
  try{
    // get localstorage data having name as info
    const serializedState=localStorage.getItem('info');
    if(serializedState === null){
      return undefined;
    }
    return JSON.parse(serializedState);
  }
  catch(err){
    return undefined;
  }
};

export const saveState=(key,state)=>{
  try{
    // serialize the state stored in the localstorage
    const serializedState = JSON.stringify(state);
    localStorage.setItem(key,serializedState);
  }
  catch(err){
  }
};
