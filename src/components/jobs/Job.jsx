import React from "react"
import {Link} from "react-router-dom";

class Job extends React.Component{
  latestJobs(id){
     this.props.displaylatestJobs(id)
  }
  render(){
    const {id,title,skills,location} =this.props
   
    return(
      <Link to={"/latest/" + id} className="noDecoration cardlist">
          <li className="list-group-item clickable"
          onClick={(e)=>this.latestJobs(e,id)}
          
          >
            <p className="mb-0"><span className="text-secondary" style={{'paddingRight':'10px'}}>Title : </span> {title}</p>
            <p className="mb-0"><span className="text-secondary" style={{'paddingRight':'10px'}}>Skills : </span> {skills.map((skil,index)=><span key={index}>{skil}, </span>)}</p>
            <p className="mb-0"><span className="text-secondary" style={{'paddingRight':'10px'}}>Location : </span> {location}</p>
          </li>
      </Link>
    );
  }
}

export default Job
