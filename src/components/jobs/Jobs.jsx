import React from "react"
import Job from "./Job"
import {connect} from "react-redux"
import {getInternalJobs, displaylatestJobs} from "../../actions/jobAction"


class Jobs extends React.Component{

  componentDidMount(){
    this.props.getInternalJobs()
  }

  render(){
    const {listOfJobs, displayLength, displayContainer, displayMainTitle} = this.props

    return(
        <div className={displayContainer !==false ? "body col-12" : "body"}>
          {displayMainTitle !==false && <h4 className="my-3">List Of all Jobs</h4>}
            <ul className="list-group">
            { listOfJobs && listOfJobs.slice(0,displayLength ? displayLength : listOfJobs.length).map((list,index)=>{
                  return <Job
                  key={index}
                  id = {list.id}
                  title = {list.title}
                  skills = {list.skills}
                  location = {list.location}
                  displaylatestJobs = {displaylatestJobs}
                  />
              })
            }
            </ul>
        </div>
    );
  }
}

function mapStateToProps(state){
  const{listOfJobs} = state.jobs
  return{
    listOfJobs
  }
}

export default connect(mapStateToProps,{getInternalJobs, displaylatestJobs})(Jobs);
