import React from "react"
import {Link} from "react-router-dom"

var Textarea = React.createClass({
    getInitialState: function() {
      return {
        condition: false
      }
    },
    handleClick: function() {
      this.setState({
        condition: !this.state.condition
      });
    },
    render: function() {
      return ( 
        <div className="form-group">
        <textarea type="text"  
        className= { this.state.condition ? "form-control fg-toggled" : "form-control" }
        name="description" 
         onClick={ this.handleClick }  
         rows="3" 
         placeholder="textarea">
         </textarea>
      </div>
    
      )    
    }
  });
