import React from 'react';
import {Provider} from 'react-redux';
import Routes from '../Routes/Routes';
import { withFirebase } from '../config/Firebase/context';
import firebase from "firebase";

class Root extends React.Component{
  
  constructor(props){
    super(props);
    this.state = {
      authUser: JSON.parse(localStorage.getItem('authUser')) || null
    };
  }

  componentWillMount() {
    firebase.auth().onAuthStateChanged(authUser => {
      authUser
        ? this.setState({ authUser }, function () {    
            localStorage.setItem('authUser', JSON.stringify(authUser))
          })
        : this.setState({ authUser: null }, function () { 
          localStorage.removeItem('authUser')
        });
    });
  }

  render(){
    const { store } = this.props
    
    return(
        <Provider store={store}>
          <div>
            <Routes 
            auth={this.state.authUser}
            />
          </div>
        </Provider>
    );
  }
}

export default withFirebase(Root);

