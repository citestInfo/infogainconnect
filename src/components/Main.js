import React from "react"
import SignOutButton from "./auth/SignOutButton"
import {Link} from "react-router-dom"

class Main extends React.Component{

render(){
  return(
    <div>
        <div className="container-fluid px-0">
          <nav className="navbar navbar-expand-lg py-1" style={{backgroundColor:'#009688'}}>
          <Link className="navbar-brand" to="/dashboard">
          <img src="/img/ig_logo.png" alt="igconnect" width="200px" height="50px" />
          </Link>


          {/* <div className="media-body h-search">
                <form className="p-relative">
                    <input type="text" className="hs-input" placeholder="Search here..." />
                    <i className="fa fa-search" style={{'position':'absolute','color':'#fff','top':'33%','bottom':'0','left':'12px'}}></i>
                </form>
            </div> */}
          <button className="navbar-toggler" type="button" data-toggle="collapse"
          data-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
          </button>

          <div className="collapse navbar-collapse ml-auto" id="navbarTogglerDemo02">
            <ul className="navbar-nav ml-auto">
              <li className="nav-item">
                  <SignOutButton / >
              </li>
            </ul>
          </div>
          </nav>
        </div>
        {this.props.children}
        <div className="container-fluid px-0 bg-light footerContainer">
        <footer className="footer footerBottom">
            <p className="text-muted text-center py-2 mb-0"><i className="fa fa-copyright" aria-hidden="true"></i> Copyright. All rights reserved to infogain.com</p>
        </footer>
        </div>
    </div>
  )
}

}

export default Main;
