import React from "react"
import {Link} from "react-router-dom"
import axios from "axios"
import moment from "moment"
import {connect} from "react-redux"
import {submitHRPost} from "../../actions/HRPostsAction"

class HRPosts extends React.Component{
  constructor(props) {
    super(props);
    this.state={
      errors : {}
    }
    this.titleRef = React.createRef();
    this.descriptionRef = React.createRef();
  }


  handleSubmit = (e) => {
      e.preventDefault();
      let id = new Date();
      id = id.toLocaleString();
      id = id.replace(/[/,: ]/g, "");

      let currentdate = new Date();
      let created_at = currentdate.toISOString();

      const data = {
        id : id,
        title : this.titleRef.current.value,
        description : this.descriptionRef.current.value,
        created_at: moment(created_at).format("DD-MM-YYYY"),
      }
      let errors ={};
      if(data.title === ""){
          errors.title = "Please fill the title"
      }
      if(data.description === ""){
          errors.description = "Please fill the description"
      }
      if(Object.keys(errors).length === 0)
      {
        this.props.submitHRPost(data)
        axios.post('http://127.0.0.1:8000/updateemail',data)
        .then(res=>
          this.setState({
          mailNotify:"mail is been sent successfully",
          loading : false
        }))
        .catch(err=>this.setState({
          mailNotify : "something went wrong",
          loading : false
        }))
        document.getElementById("hr-form").reset();
      }else{
        this.setState({
          errors
        })
      }
  }


    render(){
      const { errors} = this.state
      const {hrPostsProcessing, listOfHRPostsSuccess, listOfHRPostsError} = this.props

      return(
          <div className="container my-3">
           <div className="row">
           <div className="col-sm-12 col-md-10 offset-md-1"> <h4 className="text-center pb-2"> Post Bulk Jobs</h4></div>
          <div className="col-sm-12 col-md-10 offset-md-1 bgformcolor">
          {hrPostsProcessing && <div className="loading"></div>}
             
              <form onSubmit={this.handleSubmit} id="hr-form">
              {listOfHRPostsSuccess && <div className="alert alert-success">{listOfHRPostsSuccess}</div>}
              {listOfHRPostsError && <div className="alert alert-danger">{listOfHRPostsError}</div>}
              { errors && errors.title && <p className="text-danger mb-1">{errors.title}</p>}
              { errors && errors.description && <p className="text-danger mb-1">{errors.description}</p>}

                  <div className="form-group">
                    <input type="text" className="form-control"
                    name="title" placeholder="Enter job title"
                    ref={this.titleRef}
                    />
                  </div>
                  <div className="form-group">
                    <textarea type="text" className="form-control"
                    name="description" rows="3" placeholder="Enter job description"
                    ref={this.descriptionRef}></textarea>
                  </div>
                  
                  <div className="form-group">
                  < label className = "chooseOption" > Select Location < /label>
                      <select className="form-control">
                        <option value="null">Select Location</option>
                        <option value="bangalore">Bangalore</option>
                        <option value="Mumbai">Mumbai</option>
                        <option value="Noida">Noida</option>
                        <option value="Pune">Pune</option>
                        <option value="US">US</option>
                        <option value="All">All</option>
                      </select>
                  </div>
                  <button className="btn btn-success btn-sm float-right" type="submit" >Submit</button>
                 
                  <button className="btn btn-secondary btn-sm mx-2 float-right" type="reset">Reset</button>
              </form>
               < Link className="btn btn-primary btn-sm" to="/addmore/hradmin"
               type="button"> Add more </Link>
          </div>
          </div>
          </div>
      );
    }
}

function mapStateToProps(state){
  const {hrPostsProcessing, listOfHRPostsSuccess, listOfHRPostsError} = state.hrPosts
  return{
    hrPostsProcessing, listOfHRPostsSuccess, listOfHRPostsError
  }
}
export default connect(mapStateToProps,{submitHRPost})(HRPosts);
