import React from "react"
import { Link } from "react-router-dom";

class PostType extends React.Component{
    render(){
        const {userData} = this.props
        return(
        <div className="col-sm-2 col-md-2 col-lg-2 pr-0 mt-3">
            <div className="posRelative text-white">
                <ul className="list-group postQuestion text-center">
                    {userData && userData.role == "tech" &&
                        <li className="list-group-item">
                            <Link to="/query/ask">Post Question</Link>
                        </li>
                    }
                    {
                        userData && userData.role == "job" &&
                        <li className="list-group-item"><Link to="/post/job">Post Job</Link></li>
                    }
                    {
                        userData && userData.role == "training" &&
                        <li className="list-group-item"><Link to="/training">Post Training</Link></li>
                    }
                    {
                        userData && userData.role == "hradmin" &&
                        <li className="list-group-item"><Link to="/hr">Post HR</Link></li>
                    }
                    {
                        userData && userData.role == "issadmin" &&
                        <li className="list-group-item"><Link to="/iss">Post ISS</Link></li>
                    }
                </ul>
            </div>
        </div>
        );
    }
}

export default PostType