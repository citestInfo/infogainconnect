import React, { Component } from "react";
import { withRouter, Link } from "react-router-dom";
import { Button, Modal, Form } from "react-bootstrap";
import Updates from "../latest/Updates";

class Dashboard extends Component {
  constructor(props, context) {
    super(props, context);
    this.handleShow = this.handleShow.bind(this);
    this.handleClose = this.handleClose.bind(this);

    this.state = {
      show: false,
      open: true
    };
  }

  handleClose() {
    this.setState({ show: false });
  }

  handleShow() {
    this.setState({ show: true });
  }

  render() {

    return (
      <div className="container-fluid">
        <div className="row">
          <div className="col-md-9">
            
            <div className="col-md-12" style={{ padding: "0" }}>
              <div className="feedlistContainer">
                <div className="feedListHeder">
                  <h3 className="mb-0 pt-2">Ask Query</h3>
                </div>
                <div className="feedlists">
                  <div>
                    <div
                      className="form-group"
                      style={{ background: "#fff", borderRadius: "5px" }}
                    >
                      <textarea
                        onClick={this.handleShow}
                        className="form-control"
                        rows="4"
                        placeholder="Post here.."
                      />
                    </div>
                  </div>
                  <ul>
                    <li>Post one</li>
                    <li>post two</li>
                    <li>post three</li>
                    <li>post four</li>
                  </ul>
                  <Link to="/" className="noDecoration py-3 pull-right text-info">View More</Link>
                </div>
              </div>
            </div>
          </div>
     
            <Updates />
          

          <Modal
            show={this.state.show}
            className="modal-show"
            onHide={this.handleClose}
          >
            <Modal.Header closeButton>
              <Modal.Title style={{ paddingLeft: "10px" }}>
                Post Here
              </Modal.Title>
            </Modal.Header>
            <Modal.Body>
              <Form>
                <Form.Group controlId="exampleForm.ControlInput1">
                  <Form.Label>Email address</Form.Label>
                  <Form.Control type="email" placeholder="name@example.com" />
                </Form.Group>
                <Form.Group controlId="exampleForm.ControlSelect1">
                  <Form.Label>Example select</Form.Label>
                  <Form.Control as="select">
                    <option>1</option>
                    <option>2</option>
                    <option>3</option>
                    <option>4</option>
                    <option>5</option>
                  </Form.Control>
                </Form.Group>
                <Form.Group controlId="exampleForm.ControlSelect2">
                  <Form.Label>Example multiple select</Form.Label>
                  <Form.Control as="select" multiple>
                    <option>1</option>
                    <option>2</option>
                    <option>3</option>
                    <option>4</option>
                    <option>5</option>
                  </Form.Control>
                </Form.Group>
                <Form.Group controlId="exampleForm.ControlTextarea1">
                  <Form.Label>Example textarea</Form.Label>
                  <Form.Control as="textarea" rows="3" />
                </Form.Group>
              </Form>
            </Modal.Body>
            <Modal.Footer>
              <Button variant="secondary" onClick={this.handleClose}>
                Close
              </Button>
              <Button variant="primary" onClick={this.handleClose}>
                Save Changes
              </Button>
            </Modal.Footer>
          </Modal>
        </div>
      </div>
    );
  }
}

export default withRouter(Dashboard);
