import React from "react"
import axios from "axios"
import {connect} from "react-redux"
import {postJob} from "../../actions/jobAction"
import { Link} from "react-router-dom"

class SaveJobs extends React.Component{
  constructor(props) {
    super(props);
    this.state={
      dropdownVisible : false,
      selectedSkills : [],
      skillsAvailable : ["Reactjs","Javascript","HTML5","CSS3","Angularjs"],
      errors : {}
    }
    this.titleRef = React.createRef();
    this.descriptionRef = React.createRef();
    this.skillsRef = React.createRef();
    this.yoeRef = React.createRef();
    this.locationRef = React.createRef();
    this.imageRef = React.createRef();
  }


  handleSubmit = (e) => {
      e.preventDefault();
      const id = new Date().valueOf();
      const data = {
        id : id,
        title : this.titleRef.current.value,
        description : this.descriptionRef.current.value,
        skills : this.state.selectedSkills,
        location : this.locationRef.current.value,
        yoe : this.yoeRef.current.value,
        image: this.imageRef.current.files[0] || null
      }
      let errors ={};
      if(data.title === ""){
          errors.title = "Please fill the job title"
      }
      if(data.description === ""){
          errors.description = "Please fill the job description"
      }
      if(data.location === ""){
          errors.location = "Please fill the job location"
      }
      if(data.yoe === ""){
          errors.yoe = "Please fill the job description"
      }
      if(data.skills.length === 0){
          errors.skills = "Please select skill set required"
      }
      if(Object.keys(errors).length === 0 )
      {
        this.props.postJob(data)
        axios.post('http://127.0.0.1:8000/hr/email',data)
        .then(res=>this.setState({
          mailNotify:"mail been sent successfully",
          loading : false
        }))
        .catch(err=>this.setState({
          mailNotify : "something went wrong",
          loading : false
        }))
        document.getElementById("job-form").reset();
      }else{
        this.setState({
          errors
        })
      }
  }


  showDropdown = (e) => {
      if(e.target.value.length > 0){
        this.setState({
          dropdownVisible : true
        })
      }else{
        this.setState({
          dropdownVisible : false
        })
      }
  }

  selectSkill =(e)=>{
    const skill = e.target.getAttribute('value');
    const {selectedSkills,skillsAvailable} = this.state
    this.setState({
      selectedSkills : selectedSkills.concat(skill),
      skillsAvailable : skillsAvailable.filter((value,ind)=>value !== skill)
    })
  }

skillDelete = (e) => {
    const value = e.target.id;
    const {selectedSkills,skillsAvailable} = this.state
    this.setState({
      selectedSkills : selectedSkills.filter(skill => value !== skill),
      skillsAvailable : skillsAvailable.concat(value)
    })
}


    render(){
      const {dropdownVisible, selectedSkills, skillsAvailable, errors} = this.state
      const {jobProcessing, listOfJobsSuccess, listOfJobsError} = this.props

      return(
          <div className="container my-3">
            <div className="row">
              <div className="col-md-10  offset-md-1"> <h4 className="text-center pb-2"> Internal Job Post</h4></div>
                 <div className="col-md-10  offset-md-1 bgformcolor">
                 {jobProcessing && <div className="loading"></div>}
                 <form onSubmit={this.handleSubmit} id="job-form">
              {listOfJobsSuccess && <div className="alert alert-success">{listOfJobsSuccess}</div>}
              {listOfJobsError && <div className="alert alert-danger">{listOfJobsError}</div>}
              { errors && errors.title && <p className="text-danger mb-1">{errors.title}</p>}
              { errors && errors.description && <p className="text-danger mb-1">{errors.description}</p>}
              { errors && errors.location && <p className="text-danger mb-1">{errors.location}</p>}
              {  errors && errors.yoe && <p className="text-danger mb-1">{errors.yoe}</p>}
              

                  <div className="form-group">
                    <input type="text" className="form-control"
                    name="title" placeholder="Enter job title"
                    ref={this.titleRef}
                    />
                  </div>
                  <div className="form-group">
                    <textarea type="text" className="form-control"
                    name="description" rows="3" placeholder="Enter job description"
                    ref={this.descriptionRef}></textarea>
                  </div>
                  <div className="form-group">
                  <input type="text" className="form-control" onChange={this.showDropdown}
                  placeholder="Enter Skill"
                   />
                    {dropdownVisible &&
                          <div className="skillSetContainer">
                              {skillsAvailable && skillsAvailable.map((data,index)=>
                                <ul className="skillSet list-group" key={index}>
                                  <li className="list-group-item" value={data} onClick={this.selectSkill}>{data}</li>
                                </ul>
                              )}
                          </div>
                    }
                    {
                      selectedSkills.length > 0 && <div>
                      Skill set : {selectedSkills.map((skill,ind)=><span key={ind} className="badge badge-info mx-2">{skill} <span onClick={this.skillDelete} id={skill}><i className="fa fa-close"></i></span> </span>)}
                      </div>
                    }
                  </div>
                  
                  <div className="form-group">
                    <input type="text" className="form-control" placeholder="Enter years of experience"
                    ref={this.yoeRef}
                     />
                  </div>
                  <div className="form-group">
                    <input type="text" className="form-control" placeholder="Enter location"
                    ref={this.locationRef}
                     />
                  </div>
                  <div className="form-group">
                  <label className="chooseOption"> Select Location </label>
                      <select className="form-control">
                        <option value="null">Select Location</option>
                        <option value="bangalore">Bangalore</option>
                        <option value="Mumbai">Mumbai</option>
                        <option value="Noida">Noida</option>
                        <option value="Pune">Pune</option>
                        <option value="US">US</option>
                        <option value="All">All</option>
                      </select>
                  </div>
                  <button className="btn btn-success btn-sm float-right" type="submit" >Submit</button>
                  <button className="btn btn-secondary btn-sm mx-2 float-right" type="reset">Reset</button>
              </form>
              <Link to="/addmore/jobs" className="btn btn-primary btn-sm">
                    Add more
              </Link>
                 
                 
                 </div>
                 </div>
                 </div>
       

             
       
         
      );
    }
}

function mapStateToProps(state){
  const {jobProcessing,listOfJobs,listOfJobsError,listOfJobsSuccess} = state.jobs
  return{
    jobProcessing,
    listOfJobsError,
    listOfJobs,
    listOfJobsSuccess
  }
}
export default connect(mapStateToProps,{postJob})(SaveJobs);
