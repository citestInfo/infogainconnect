import React from "react"
import firebase from "../../config/Firebase"
import axios from "axios"
import moment from "moment"
import { connect } from "react-redux"
import { submitAddmorePost } from "../../actions/AddMorePostAction"

class AddMore extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            errors: {},
            loading : false,
            mailNotify : ""
        }
        this.titleRef = React.createRef();
        this.descriptionRef = React.createRef();
        this.typeRef = React.createRef();
        this.imageRef = React.createRef();
    }

    handleSubmit = (e) => {
        e.preventDefault();
        let id = new Date();
        id = id.toLocaleString();
        id = id.replace(/[/,: ]/g, "");

        let currentdate = new Date();

        let created_at = currentdate.toISOString();
        const data = {
            id: id,
            title: this.titleRef.current.value,
            description: this.descriptionRef.current.value,
            type: this.typeRef.current.value,
            image: this.imageRef.current.files[0],
            created_at: moment(created_at).format("DD-MM-YYYY"),
        }
        let errors = {};
        if (data.title === "") {
            errors.title = "Please fill the title"
        }
        if (data.description === "") {
            errors.description = "Please fill the description"
        }
        if (data.image === "") {
            errors.image = "Please upload the image"
        }
        if (Object.keys(errors).length === 0) {
            var storage = firebase.storage();
            var file = this.imageRef.current.files[0];
            var storageRef = firebase.storage().ref();
            //dynamically set reference to the file name
            var thisRef = storageRef.child(file.name);
            //put request upload file to firebase storage
            thisRef.put(file).then(function (snapshot) {
                console.log('Uploaded a blob or file!');
            });
            const that = this;
            //get request to get URL for uploaded file
            thisRef.getDownloadURL().then(function (url) {
                data.image = url;
                that.props.submitAddmorePost(data)
                axios.post('http://127.0.0.1:8000/addmore/email', data)
                    .then(res => that.setState({
                        mailNotify: "mail sent successfully",
                        loading: false
                    }))
                    .catch(err => that.setState({
                        mailNotify: "something went wrong",
                        loading: false
                    }))
                document.getElementById("addmore").reset();
            })
        }else{
            this.setState({
                errors
            })
        }
    }


    render() {
        const { errors } = this.state
        const { postFetching, postSuccess, postError } = this.props
        
        return (
            <div className="container my-3">
                {postFetching && <div className="loading"></div>}
                <h4 className="text-center pb-2"> Post with image </h4>
                <form onSubmit={this.handleSubmit} id="addmore">
                    {postSuccess && <div className="alert alert-success">{postSuccess}</div>}
                    {postError && <div className="alert alert-danger">{postError}</div>}
                    {errors && errors.title && <p className="text-danger mb-1">{errors.title}</p>}
                    {errors && errors.description && <p className="text-danger mb-1">{errors.description}</p>}
                    {errors && errors.image && <p className="text-danger mb-1">{errors.image}</p>}

                    <div className="form-group">
                        <input type="text" className="form-control"
                            name="title" placeholder="Enter title"
                            ref={this.titleRef}
                        />
                    </div>
                    <div className="form-group">
                        <textarea type="text" className="form-control"
                            name="description" rows="3" placeholder="Enter description"
                            ref={this.descriptionRef}></textarea>
                    </div>
                    <div className="form-group">
                        <input type="file" name="banner" ref={this.imageRef}
                        className="form-control"
                        />
                    </div>
                    <div className="form-group">
                        <input type="text" name="type" ref={this.typeRef}
                        className="form-control" readOnly defaultValue={this.props.match.params.type}
                        />
                    </div>
                    <div className="form-group">
                        <label className="chooseOption"> Select Location </label>
                        <select className="form-control">
                            <option value="null">Select Location</option>
                            <option value="bangalore">Bangalore</option>
                            <option value="Mumbai">Mumbai</option>
                            <option value="Noida">Noida</option>
                            <option value="Pune">Pune</option>
                            <option value="US">US</option>
                            <option value="All">All</option>
                        </select>
                    </div>
                    <button className="btn btn-success btn-sm float-right" type="submit" >Submit</button>

                    <button className="btn btn-secondary btn-sm mx-2 float-right" type="reset">Reset</button>
                </form>
                < button className="btn btn-primary btn-sm" onClick={()=>this.props.history.goBack()}
                    type="button"> Go Back </button>
            </div>
        );
    }
}

function mapStateToProps(state) {
    const { postFetching, postSuccess, postError} = state.addMorePosts
    
    return {
        postFetching, postSuccess, postError
    }
}
export default connect(mapStateToProps, { submitAddmorePost })(AddMore);
