import React from 'react'
import Jobs from '../jobs/Jobs'
import { Link, withRouter } from "react-router-dom";
import { latestRef } from "../../config/Firebase"
import { connect } from "react-redux"
import { getLatest, displaylatest, getTrendingTech } from "../../actions/latestAction"
import { getQuestions, getQuestion } from "../../actions/questionsAction"
import { getTrainings } from "../../actions/trainingAction"
import { getHRPosts } from "../../actions/HRPostsAction"
import { getISSPosts } from "../../actions/ISSPostsAction"
import { getCurrentAuth } from "../../actions/userAction"
import { Button, Modal, Form, Collapse, Fade } from "react-bootstrap";

class Updates extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            data: [],
            latest: [],
            loading: true,
            show: false,
            open: true,
            open1: false,
            open2: false,
            open3: false
        }
        this.latestData = this.latestData.bind(this);
    }

    componentDidMount() {
        this.latestData();
        this.props.getTrainings();
        this.props.getHRPosts();
        this.props.getISSPosts();
        this.props.getCurrentAuth();
        // this.props.getTrendingTech();
        var lref = latestRef;
        lref.on("value", function (snapshot) {
            const latestArray = Object.keys(snapshot.val()).map(i => snapshot.val()[i]);
            this.setState({
                latest: latestArray[0],
                loading: false
            });
        }.bind(this), function (error) {
            console.log("Error: " + error.code);
        });
    }

    componentDidUpdate(nextProps) {
        if (this.props.match.params.id !== nextProps.match.params.id) {
            this.latestData();
            this.props.getTrainings();
            this.props.getHRPosts();
            this.props.getISSPosts();
        }
    }

    latestData() {
        this.props.getLatest()
    }

    render() {
        const { open, open1, open2, open3 } = this.state;
       const { listOfJobs, listOfTrainings, listOfHRPosts, listOfIssPosts, currentUserData } = this.props
       
       return(
           <div className="col-sm-3 col-md-3 col-lg-3">
               <ul className="list-group postQuestion text-center">
                   {currentUserData && currentUserData.role == "tech" &&
                       <li className="list-group-item">
                           <Link to="/query/ask">Post Question</Link>
                       </li>
                   }
                   {
                       currentUserData && currentUserData.role == "job" &&
                       <li className="list-group-item"><Link to="/post/job">Post Job</Link></li>
                   }
                   {
                       currentUserData && currentUserData.role == "training" &&
                       <li className="list-group-item"><Link to="/training">Post Training</Link></li>
                   }
                   {
                       currentUserData && currentUserData.role == "hradmin" &&
                       <li className="list-group-item"><Link to="/hr">Post HR</Link></li>
                   }
                   {
                       currentUserData && currentUserData.role == "issadmin" &&
                       <li className="list-group-item"><Link to="/iss">Post ISS</Link></li>
                   }
               </ul>
               {/* <div className="latestUpdate my-2 ">
                   <div className="header p-2 bg-secondary text-white text-center">
                       Internal Job Posting
                </div>
                   <Jobs
                       displayLength={5}
                       displayContainer={false}
                       displayMainTitle={false}
                   />
                   <div className="footer">
                       {listOfJobs && listOfJobs.length > 5 &&
                           <Link to="/jobs" className="text-primary  noDecoration float-right pb-2">View More Jobs</Link>
                       }
                   </div>
               </div>
               <div className="latestUpdate my-3 ">
                   <div className="header p-2 bg-secondary text-white text-center">
                       Infogain Training
                </div>
                   <div className="body">
                       <ul className="list-group">
                           {listOfTrainings && listOfTrainings.slice(0, 5).map((list, index) => {
                               return <Link to={"/latest/" + list.id} className="noDecoration" key={index}>
                                   <li className="list-group-item clickable" key={index}>{list.title}</li>
                               </Link>
                           })
                           }
                       </ul>
                   </div>
                   <div className="footer">
                       {listOfTrainings && listOfTrainings.length > 5 &&
                           <Link to="/trainings" className="text-primary  noDecoration float-right pb-2">View More training</Link>
                       }
                   </div>
               </div>
               <div className="latestUpdate my-3 ">
                   <div className="header p-2 bg-secondary text-white text-center">
                       HR Admin Updates
                </div>
                   <div className="body">
                       <ul className="list-group">
                           {listOfHRPosts && listOfHRPosts.slice(0, 5).map((list, index) => {
                               return <Link to={"/latest/" + list.id} className="noDecoration" key={index}>
                                   <li className="list-group-item clickable" key={index}>{list.title}</li>
                               </Link>
                           })
                           }
                       </ul>
                   </div>
                   <div className="footer">
                       {listOfHRPosts && listOfHRPosts.length > 5 &&
                           <Link to="/hradmin" className="text-primary  noDecoration float-right pb-2">View More HR updates</Link>
                       }
                   </div>
               </div>
               <div className="latestUpdate my-3 ">
                   <div className="header p-2 bg-secondary text-white text-center">
                       ISS Admin Updates
                </div>
                   <div className="body">
                       <ul className="list-group">
                           {listOfIssPosts && listOfIssPosts.slice(0, 5).map((list, index) => {
                               return <Link to={"/latest/" + list.id} className="noDecoration" key={index}>
                                   <li className="list-group-item clickable" key={index}>{list.title}</li>
                               </Link>
                           })
                           }
                       </ul>
                   </div>
                   <div className="footer">
                       {listOfIssPosts && listOfIssPosts.length > 5 &&
                           <Link to="/issadmin" className="text-primary  noDecoration float-right pb-2">View More ISS updates</Link>
                       }
                   </div> */}
                <div className="sidebarcontainer">
              <a
                onClick={() => this.setState({ open: !open })}
                aria-controls="example-fade-text"
                aria-expanded={open}
              >
               Internal Job Posting
              </a>
              <Fade in={this.state.open}>
                <div id="example-fade-text" className="collapsecontent">
                   <Jobs
                       displayLength={5}
                       displayContainer={false}
                       displayMainTitle={false}
                   />
                   <div className="footer">
                       {listOfJobs && listOfJobs.length > 5 &&
                           <Link to="/jobs" className="text-primary  noDecoration float-right pb-2">View More Jobs</Link>
                       }
                   </div>
                </div>
              </Fade>
              <a
                onClick={() => this.setState({ open1: !open1 })}
                aria-controls="example-fade-text1"
                aria-expanded={open1}
              >
               Infogain Training
              </a>
              <Fade in={this.state.open1}>
                <div id="example-fade-text1" className="collapsecontent">
                 <div className="body">
                       <ul className="list-group">
                           {listOfTrainings && listOfTrainings.slice(0, 5).map((list, index) => {
                               return <Link to={"/latest/" + list.id} className="noDecoration" key={index}>
                                   <li className="list-group-item clickable" key={index}>{list.title}</li>
                               </Link>
                           })
                           }
                       </ul>
                   </div>
                   <div className="footer">
                       {listOfTrainings && listOfTrainings.length > 5 &&
                           <Link to="/trainings" className="text-primary  noDecoration float-right pb-2">View More training</Link>
                       }
                   </div>
                </div>
              </Fade>
              <a
                onClick={() => this.setState({ open2: !open2 })}
                aria-controls="example-fade-text1"
                aria-expanded={open2}
              >
                HR Admin Updates
              </a>
              <Fade in={this.state.open2}>
                <div id="example-fade-text1" className="collapsecontent">
            <div className="body">
                       <ul className="list-group">
                           {listOfHRPosts && listOfHRPosts.slice(0, 5).map((list, index) => {
                               return <Link to={"/latest/" + list.id} className="noDecoration" key={index}>
                                   <li className="list-group-item clickable" key={index}>{list.title}</li>
                               </Link>
                           })
                           }
                       </ul>
                   </div>
                   <div className="footer">
                       {listOfHRPosts && listOfHRPosts.length > 5 &&
                           <Link to="/hradmin" className="text-primary  noDecoration float-right pb-2">View More HR updates</Link>
                       }
                   </div>
                </div>
              </Fade>
                      <a
                onClick={() => this.setState({ open3: !open3 })}
                aria-controls="example-fade-text1"
                aria-expanded={open3}
              >
                 ISS Admin Updates
              </a>
              <Fade in={this.state.open3}>
                <div id="example-fade-text1" className="collapsecontent">
                <div className="body">
                       <ul className="list-group">
                           {listOfIssPosts && listOfIssPosts.slice(0, 5).map((list, index) => {
                               return <Link to={"/latest/" + list.id} className="noDecoration" key={index}>
                                   <li className="list-group-item clickable" key={index}>{list.title}</li>
                               </Link>
                           })
                           }
                       </ul>
                   </div>
                   <div className="footer">
                       {listOfIssPosts && listOfIssPosts.length > 5 &&
                           <Link to="/issadmin" className="text-primary  noDecoration float-right pb-2">View More ISS updates</Link>
                       }
                   </div>
                   </div>
              </Fade>
              
            </div>
               </div>
        
         
       );
   }
}


function mapStateToProps(state) {
    const { jobProcessing, listOfJobs } = state.jobs
    const { trainingProcessing, listOfTrainings } = state.training
    const { listOfHRPosts } = state.hrPosts
    const { listOfIssPosts } = state.issPosts
    const { currentUserData } = state.users

    return {
        jobProcessing,
        listOfJobs,
        trainingProcessing,
        listOfTrainings,
        listOfHRPosts,
        listOfIssPosts,
        currentUserData
    }
}

export default withRouter(connect(mapStateToProps, {
    getLatest, getQuestion, getQuestions, getTrainings, getHRPosts,
    getISSPosts, displaylatest, getTrendingTech, getCurrentAuth
})(Updates));