import React from "react"
import {connect} from "react-redux"
import {updateTech} from "../../actions/latestAction"

class Blog extends React.Component{

  componentDidMount(){
    this.props.updateTech(this.props.match.params.author)
  }

  render(){
    const {tech, latestUpdateFetching} =this.props
    return (
      <div className="row mx-0">
      {latestUpdateFetching && <div className="loading"></div>}
      {tech && tech.map((latest,index)=>
        <div className="col-sm-12 col-md-12 col-lg-12 text-center" key={index}>
            <h4 className="mt-3">{latest.title}</h4>
            <hr/>
              <div>{latest.description}</div>
        </div>
      )}
      </div>
    );
  }
}

function mapStateToProps(state){
  const {tech} = state.latestUpdates
    return{
      tech
    }
}

export default connect(mapStateToProps,{updateTech})(Blog)
