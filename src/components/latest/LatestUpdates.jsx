import React from "react"


const LatestUpdates = ({data}) => {
  return(
    <React.Fragment>
      {data && data.map((latest,index)=>

        <div className="col-sm-12 col-md-12 col-lg-12 text-center" 
        style={{height:'calc(100vh - 210px)'}} key={index}>

            <h4 className="mt-3">{latest.title}</h4>
            <hr/>
            {<div className="h-100">{latest.description}</div>}
        </div>
      )}
    </React.Fragment>
  );
}

export default LatestUpdates;
