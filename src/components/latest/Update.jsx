import React from "react";
import {Link} from "react-router-dom";

class Update extends React.Component{
  latestUpdates(id){
     this.props.displayUpdates(id)
  }
  render(){
    const {title,description,id} =this.props
    return(
      <Link to={"/latest/" + id} className="noDecoration cardlist">
          <li className="list-group-item clickable"
          onClick={(e)=>this.latestUpdates(e,id)}
          >
            <p className="mb-0"><span className="text-secondary">Title : </span> {title}</p>
            <p className="mb-0"><span className="text-secondary">Description : </span> {description.length > 100 ? description.substring(0, 100)+" ....." : description}</p>
          </li>
      </Link>
    );
  }
}

export default Update
