import React from "react"
import { connect } from "react-redux"
import { displayAddMorePost } from "../../actions/AddMorePostAction"

class AddMorePost extends React.Component {

    componentDidMount() {
        this.props.displayAddMorePost(this.props.match.params.id)
    }

    render() {
        const { post, postError, postFetching } = this.props
        
        return (
            <div className="row mx-0">
                {postFetching && <div className="loading"></div>}
                {postError && <div className="alert alert-danger">{postError}</div> }

                {post && post.map((latest, index) =>
                    <div className="col-sm-12 col-md-12 col-lg-12 text-center" style={{ height: 'calc(100vh - 210px)' }} key={index}>
                        <h4 className="mt-3">{latest.title}</h4>
                        <hr />
                        <div>
                            <div><span className="text-secondary"></span>{latest.description}</div>
                            <img src={latest.image} alt="banner" width="100%" height="400px" />
                        </div>
                    </div>
                )}
            </div>
        );
    }
}

function mapStateToProps(state) {
    const { post, postError, postFetching } = state.addMorePosts
    return {
        post, postError, postFetching
    }
}

export default connect(mapStateToProps, { displayAddMorePost})(AddMorePost)
