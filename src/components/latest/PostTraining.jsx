import React from "react"
import {Link} from "react-router-dom"
import axios from "axios"
import moment from "moment"
import {connect} from "react-redux"
import {postTraining} from "../../actions/trainingAction"

class PostTraining extends React.Component{
  constructor(props) {
    super(props);
    this.state={
      errors : {}
    }
    this.titleRef = React.createRef();
    this.descriptionRef = React.createRef();
    this.dateRef = React.createRef();
    this.facultyRef = React.createRef();
    this.modeRef = React.createRef();
  }


  handleSubmit = (e) => {
      e.preventDefault();
      let id = new Date();
      id = id.toLocaleString();
      id = id.replace(/[/,: ]/g, "");
      const data = {
        id : id,
        title : this.titleRef.current.value,
        description : this.descriptionRef.current.value,
        date : moment(this.dateRef.current.value).format("DD-MM-YYYY"),
        faculty : this.facultyRef.current.value || "",
        mode : this.modeRef.current.value || "",
      }
      let errors ={};
      if(data.title === ""){
          errors.title = "Please fill the job title"
      }
      if(data.description === ""){
          errors.description = "Please fill the job description"
      }
      if(data.date === ""){
          errors.date = "Please fill the event date"
      }
      if(Object.keys(errors).length === 0 )
      {
        this.props.postTraining(data)
        axios.post('http://127.0.0.1:8000/updateemail',data)
        .then(res=>this.setState({
          mailNotify:"mail is been sent successfully",
          loading : false
        }),function(){
          this.props.history.push('/question/'+ id)
        })
        .catch(err=>this.setState({
          mailNotify : "something went wrong",
          loading : false
        }))
        document.getElementById("job-form").reset();
      }else{
        this.setState({
          errors
        })
      }
  }


    render(){
      const { errors} = this.state
      const {trainingProcessing, listOfTrainingSuccess, listOfTrainingError} = this.props

      return(
          <div className="container my-3">
          <div className="row">
          <div className="col-md-10 offset-md-1"> <h4 className="text-center pb-2"> Training</h4></div>
          <div className="col-md-10 offset-md-1  bgformcolor">
          {trainingProcessing && <div className="loading"></div>}
             
             <form onSubmit={this.handleSubmit} id="job-form">
             {listOfTrainingSuccess && <div className="alert alert-success">{listOfTrainingSuccess}</div>}
             {listOfTrainingError && <div className="alert alert-danger">{listOfTrainingError}</div>}
             { errors && errors.title && <p className="text-danger mb-1">{errors.title}</p>}
             { errors && errors.description && <p className="text-danger mb-1">{errors.description}</p>}
             { errors && errors.date && <p className="text-danger mb-1">{errors.date}</p>}

                 <div className="form-group">
                   <input type="text" className="form-control"
                   name="title" placeholder="Enter job title"
                   ref={this.titleRef}
                   />
                 </div>
                 <div className="form-group">
                   <textarea type="text" className="form-control"
                   name="description" rows="3" placeholder="Enter job description"
                   ref={this.descriptionRef}></textarea>
                 </div>
                 <div className="form-group">
                     <input type="date" className="form-control" placeholder="select scheduled date"
                     ref={this.dateRef}
                     />
                 </div>
                 <div className="form-group">
                   <input type="text" className="form-control" placeholder="Enter faculty(optional)"
                   ref={this.facultyRef}
                    />
                 </div>
                 <div className="form-group">
                  <label className="chooseOption"> Online Self learning </label>
                     <select name="mode" ref={this.modeRef} className="form-control">
                       <option value="Online Self learning">Online Self learning</option>
                       <option value="Webinar">Webinar</option>
                       <option value="Classroom">Classroom</option>
                     </select>
                 </div>

                 <div className="form-group">
                  <label className="chooseLocation"> Select Location </label>
                     <select className="form-control">
                       <option value="null">Select Location</option>
                       <option value="bangalore">Bangalore</option>
                       <option value="Mumbai">Mumbai</option>
                       <option value="Noida">Noida</option>
                       <option value="Pune">Pune</option>
                       <option value="US">US</option>
                       <option value="All">All</option>
                     </select>
                 </div>
                 <button className="btn btn-success btn-sm float-right" type="submit" >Submit</button>
                 <button className="btn btn-secondary btn-sm mx-2 float-right" type="reset">Reset</button>
             </form>
             <Link to="/addmore/training" className="btn btn-primary btn-sm">
                 Add more
             </Link>
          </div>
          
          </div>
     
          </div>
      );
    }
}

function mapStateToProps(state){
  const {trainingProcessing, listOfTrainingSuccess,listOfTrainingError} = state.training
  return{
    trainingProcessing,
    listOfTrainingError,
    listOfTrainingSuccess
  }
}
export default connect(mapStateToProps,{postTraining})(PostTraining);
