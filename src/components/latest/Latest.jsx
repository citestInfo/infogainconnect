import React from "react"
import LatestUpdates from "./LatestUpdates"
import {connect} from "react-redux"
import {displaylatest} from "../../actions/latestAction"
import {displaylatestJobs} from "../../actions/jobAction"
import {displaylatestTraining} from "../../actions/trainingAction"
import {displayISS} from "../../actions/ISSPostsAction"
import {displayHR} from "../../actions/HRPostsAction"

class Latest extends React.Component{

  componentDidMount(){
    this.props.displaylatest(this.props.match.params.id)
    this.props.displaylatestJobs(this.props.match.params.id)
    this.props.displaylatestTraining(this.props.match.params.id)
    this.props.displayHR(this.props.match.params.id)
    this.props.displayISS(this.props.match.params.id)
  }

    render(){
      const {latestTraining, latestUpdateFetching,
              latestJob, IssPost,HRPost} = this.props

      return (
        <div className="row mx-0">
        {latestUpdateFetching && <div className="loading"></div>}
          <div className="col-sm-12 col-md-10  offset-md-1 col-lg-10  offset-lg-1 px-0 mb-3">
            <button className="btn btn-sm btn-primary pull-right noDecoration "
              onClick={() => this.props.history.goBack()}>Back</button>
          </div>
        <LatestUpdates
          data = {latestTraining}
        />
        <LatestUpdates
          data = {IssPost}
        />
        <LatestUpdates
          data = {HRPost}
        />
          
        {latestJob && latestJob.map((latest,index)=>
          <div className="col-sm-12 offset-md-1 col-md-10 col-lg-10 text-center" style={{height:'calc(100vh - 210px)','background':'rgba(238, 238, 238, 0.27)'}} key={index}>
              <h4 className="mt-3">{latest.title}</h4>
              <hr/>
              <div>
                <div><span className="text-secondary" style={{'paddingRight':'10px'}}>Skills : </span>{latest.skills.map((skill,ind)=><span key={ind}>{skill}, </span>)}</div>
                <div><span className="text-secondary" style={{'paddingRight':'10px'}}>Location : </span>{latest.location}</div>
                <div><span className="text-secondary" style={{'paddingRight':'10px'}}>Job Description : </span>{latest.description}</div>
              </div>
            
          </div>
        )}
          
        </div>
      );
    }
}

function mapStateToProps(state){
  const {latestUpdate, latestUpdateFetching} = state.latestUpdates
  const {latestJob, latestJobError} = state.jobs
  const {latestTraining, latestTrainingError} = state.training
  const {IssPost, IssPostError, IssPostSuccess} = state.issPosts
  const {HRPost, HRPostError, HRPostSuccess} = state.hrPosts
  return{
      latestUpdate,latestUpdateFetching,
      latestJob,latestJobError,
      latestTraining,latestTrainingError,
      IssPost, IssPostError, IssPostSuccess,
      HRPost, HRPostError, HRPostSuccess
  }
}

export default connect(mapStateToProps,{displaylatest, displaylatestJobs, displayISS, displayHR, displaylatestTraining})(Latest)
