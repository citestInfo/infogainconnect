import React from "react"
import Update from "./Update"
import {connect} from "react-redux"
import {getHRPosts, displayHR} from "../../actions/HRPostsAction"
import {getISSPosts, displayISS} from "../../actions/ISSPostsAction"
import {displaylatestTraining, getTrainings} from "../../actions/trainingAction"

class AllUpdates extends React.Component{
  constructor(props){
    super(props);
    this.state={
      title : ""
    }
  }

  componentDidMount(){
    const path = this.props.location.pathname;
    switch(path){
        case "/hradmin":this.props.getHRPosts()
                        this.setState({
                          title : "HR Updates"
                        })
                        break;
        case "/issadmin":this.props.getISSPosts()
                        this.setState({
                          title : "ISS Updates"
                        })
                        break;
        default:this.props.getTrainings()
                  this.setState({
                    title : "Trainings"
                  })
                  break;
    }
  }

  render(){
    const {listOfTrainings, listOfHRPosts, listOfIssPosts, displayLength,
            displayContainer, displayMainTitle} = this.props
            const {title} = this.state
    return(
        <div className={displayContainer !==false ? "body" : "body"}>
        <div className="container">
        <div className="col-md-10 offset-md-1"> {displayMainTitle !==false && <h4 className="my-3">List Of all {title} </h4>}</div>
        <div className="col-md-10 offset-md-1 bgformcolor">
       
            <ul className="list-group">
            { listOfTrainings && listOfTrainings.slice(0,displayLength ? displayLength : listOfTrainings.length).map((list,index)=>{
                  return <Update
                  key={index}
                  title = {list.title}
                  description = {list.description}
                  id={list.id}
                  displayUpdates = {displaylatestTraining}
                  />
              })}

            { listOfHRPosts && listOfHRPosts.slice(0,displayLength ? displayLength : listOfHRPosts.length).map((list,index)=>{
                  return <Update
                  key={index}
                  title = {list.title}
                  description = {list.description}
                  id={list.id}
                  displayUpdates = {displayHR}
                  />
              })}

            { listOfIssPosts && listOfIssPosts.slice(0,displayLength ? displayLength : listOfIssPosts.length).map((list,index)=>{
                  return <Update
                  key={index}
                  title = {list.title}
                  description = {list.description}
                  id={list.id}
                  displayUpdates = {displayISS}
                  />
              })
            }
            </ul>
        
        </div>
        
        </div>
       

        </div>
    );
  }
}

function mapStateToProps(state){
  const {listOfTrainings} = state.training
  const {listOfHRPosts} = state.hrPosts
  const {listOfIssPosts} = state.issPosts
  return{
    listOfTrainings,
    listOfHRPosts,
    listOfIssPosts
  }
}

export default connect(mapStateToProps,{getHRPosts,
displayHR,
getISSPosts,
displayISS,
displaylatestTraining,
getTrainings})(AllUpdates);
