import React from "react"
import {withRouter} from "react-router-dom";
import {dataRef} from "../../config/Firebase"
import {connect } from "react-redux"
import {updateQuestion} from "../../actions/questionsAction"


class EditQuestion extends React.Component{
constructor(props){
  super(props);
  this.state={
    data:[],
    loading : true
  }
  this.inputChange = this.inputChange.bind(this);
  this.getData = this.getData.bind(this);
}

componentDidMount(){
  this.getData();
}

  componentDidUpdate(nextProps){
    if(this.props.match.params.id !== nextProps.match.params.id){
    this.getData();
    }
  }

getData(){
  var ref = dataRef;
  ref.on("value", function(snapshot) {
     const peopleArray = Object.keys(snapshot.val()).map(i => snapshot.val()[i]);
      this.setState({
        data : peopleArray.filter((list,i)=>list._id === this.props.match.params.id),
        loading:false,
      });
  }.bind(this), function (error) {
     console.log("Error: " + error.code);
  });
}

inputChange(e){
  let name = e.target.name;
  let value = e.target.value;
  const updatedArray = [...this.state.data];
  updatedArray[0] = {
                      ...updatedArray[0],
                      [name]:value
                    }
  this.setState({
       data: [
              updatedArray[0]
             ]
   });
}

handleSubmit = (e) => {
  e.preventDefault();
  this.setState({
    loading : true
  },
  function(){
    this.props.updateQuestion(this.state.data[0],this.state.data[0]._id)
    this.props.history.goBack()

    // var ref = dataRef;
    // ref.orderByChild('_id').equalTo(this.state.data[0]._id).on("value", function(snapshot) {
    //   snapshot.forEach(function(child) {
    //       ref.child(child.key).update(this.state.data[0])
    //   }.bind(this));
    // }.bind(this))
  })
}
// );
    // console.log(this.state);
    // function(){
    //   var ref = dataRef;
    //   ref.push().set(this.state.data[0])
    //   this.getData()
    // }

//   )
// }

  render(){
    const {data,loading} = this.state
    return(
      <div className="container my-3">
          {loading && <div className="loading"></div>}
          <h4 className="text-center pb-2">Edit Question</h4>
          <form onSubmit={this.handleSubmit}>
            {data.map((dat,index)=>
              <div className="row mx-0" key={index}>
                <div className="col-sm-12 col-md-12 col-lg-12">
                  <div className="form-group">
                    <input type="text" className="form-control" name="title"
                    onChange={this.inputChange}
                    defaultValue={dat.title}
                    placeholder="Enter title"
                    />
                  </div>
                </div>
                <div className="col-sm-12 col-md-12 col-lg-12">
                  <div className="form-group">
                    <textarea type="text" className="form-control" name="question"
                      onChange={this.inputChange}
                      defaultValue={dat.question}
                      rows="5"
                      placeholder="Enter description"
                    />
                  </div>
                </div>
                <div className="col-sm-12 col-md-12 col-lg-12">
                  <div className="form-group">
                    <textarea type="text" className="form-control" name="code"
                      onChange={this.inputChange}
                      defaultValue={dat.code}
                      rows="5"
                      placeholder="Enter code"
                    />
                  </div>
                </div>

                <div className="col-sm-12 col-md-12 col-lg-12">
                  <div className="form-group">
                      <label className = "chooseOption"> Select Project </label>
                      <select name="project" defaultValue={dat.project} className="form-control">
                        <option value="project 1">Project 1</option>
                        <option value="project 2">Project 2</option>
                        <option value="project 3">Project 3</option>
                      </select>
                  </div>
                </div>
                <div className="col-sm-12 col-md-12 col-lg-12">
                <button className="btn btn-success  float-right" type="submit">Submit</button>
                <button className="btn btn-dark mx-2 float-right" type="reset">Reset</button>
                </div>
              </div>
            )}
          </form>
      </div>
    );
  }
}

export default withRouter(connect(null,{updateQuestion})(EditQuestion));
