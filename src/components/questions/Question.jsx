import React from 'react'
import {Link} from "react-router-dom";
import {dataRef} from "../../config/Firebase"
import {connect} from "react-redux"
import {getQuestions,getQuestion} from "../../actions/questionsAction"
import Updates from "../latest/Updates"
import PostType from "../posts/PostType";

class Question extends React.Component{
  constructor(props){
    super(props);
    this.state={
      data : [],
      latest : [],
      loading : true
    }
    this.answerRef = React.createRef();
    this.getData = this.getData.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

componentDidMount(){
  this.getData();
}

  componentDidUpdate(nextProps){
    if(this.props.match.params.id !== nextProps.match.params.id){
      this.getData();
    }
  }


  getData(){
    let id = this.props && this.props.match.params.id
    this.props.getQuestion(id)
  }

  handleSubmit(e){
      e.preventDefault();

      let id = new Date();
      id = id.toLocaleString();
      id = id.replace(/[/,: ]/g, "");
      const question_id = this.props && this.props.match.params.id;
      const data = {
        answer : this.answerRef.current.value,
        name : "authenticated_user",
        code : "",
        helped : false,
        id:id,
        likes:0,
        solved:0
      }


      var ref = dataRef;
      ref.orderByChild('_id').equalTo(question_id).once("value", function(snapshot) {
        snapshot.forEach(function(child) {
            // ref.child(child.key).child("answers").setValue(data) //not a function
            // ref.child(child.key).child("answers").update(data) // object is not created, just added
            ref.child(child.key).child("answers").push().set(data) // object is created, but parent is converted from array to object
            // ref.child(child.key).child("answers").push(data) // object is created, but parent is converted from array to object
            // ref.child(child.key).child("answers").set(data) // object is not created, just added
            // ref.child(child.key).child("answers").
        });
      })

//       var ref = firebase.database().ref();
// ref.child("Posts").orderByChild("code").equalTo("12345TG4").once("value", function(snapshot) {
//     snapshot.forEach(function(child) {
//         child.ref.update(updateData);
//     });
// });
  }

    render(){
      const {questionFetching,question} = this.props
      const id = this.props.match.params.id;
      
      return(
          <div className="row mx-0">
            {questionFetching && <div className="loading"></div>}
            <div className="col-sm-9 col-md-9 col-lg-9">
              {question && question.map((val,index)=>
                <div className=" p-2 my-3" key={index}>
                  <h3 className="title">
                    {val.title}
                  </h3>
                  <p>
                    {val.question}
                  </p>
                  <div className="row mx-0">
                    <code className="codeLayout">
                    {val.code}
                    </code>
                    <div className="col-sm-6  col-md-6 col-lg-6 px-0">
                        <p className="mb-1 my-2"><span className="text-primary p-1">Tags: </span>
                        {
                          val.tags.map((tag,index)=>
                          <span className="text-secondary questionTags my-2" key={index}>{tag};</span>
                        )}
                        </p>
                    </div>
                    <div className="col-sm-6  col-md-6 col-lg-6 px-0">
                    { val.status===1 ?
                      <span className="float-right mx-2  my-2 text-success"><i className="fa fa-smile-o" aria-hidden="true"></i> Closed </span>
                    :  <span className="float-right mx-2  my-2 text-danger"><i className="fa fa-frown-o" aria-hidden="true"></i> Open </span>
                    }
                    <span className="float-right mx-2 my-2 clickable"><Link to={"/questions/edit/"+ id}><i className="fa fa-pencil" aria-hidden="true"></i> Edit</Link></span>
                    <span className="float-right mx-2 my-2 "><i className="fa fa-check"></i> {val.answers && val.answers.length} answers</span>
                    <span className="float-right my-2 "><i className="fa fa-eye"></i> {val.views} views</span>
                    </div>
                  </div>
                  <div className="row mx-0">

                    <h4 className="mt-3 mb-0">Answers</h4>

                  { Object.keys(val.answers).map((ans,ind)=>
                        <div className="row mx-0 mt-3 w-100" key={ind}>
                          <div className="col-sm-12 col-md-12 col-lg-12 px-0">
                            <span className="mb-0 text-left"><i>{val.answers[ans].name} </i>says :</span>
                            <span className="mb-0 float-right">
                            {val.answers[ans].id===1 &&
                              <span className=" mx-2 my-2 clickable"><Link to={"/questions/edit/" + id}><i className="fa fa-pencil" aria-hidden="true"></i> Edit</Link></span>
                            }
                              <i className="fa fa-thumbs-up" aria-hidden="true"></i> {val.answers[ans].likes} likes
                              <button className={val.answers[ans].helped=== true ? "btn btn-info disabled btn-sm ml-2" : "btn btn-outline-info btn-sm ml-2"}><i className="fa fa-bookmark" aria-hidden="true"></i> Helpful</button>
                              {val.answers[ans].solved === true ?
                                <button className="btn btn-success disabled btn-sm ml-2"><i className="fa fa-check" aria-hidden="true"></i> Solved</button>
                                :
                                <button className="btn btn-outline-success btn-sm ml-2"><i className="fa fa-check" aria-hidden="true"></i> Solved</button>
                              }
                            </span>
                          </div>
                          <div className="col-12  py-3 bg-light mt-2 mb-3" >
                            <p>{val.answers[ans].answer}</p>
                            {val.answers[ans].code &&
                              <code className="codeLayout">
                              {val.answers[ans].code}
                              </code>
                            }
                          </div>
                        </div>
                  )}
                  </div>
                  <div className="row mx-0">
                    <div className="col-12 px-0">
                      <form onSubmit={this.handleSubmit}>
                        <div className="form-group">
                            <textarea ref={this.answerRef} className="form-control" name="answer" rows="5" placeholder="your answer"></textarea>
                        </div>
                        <button className="btn btn-success btn-sm float-right">Post Your Answer</button>
                      </form>
                    </div>
                  </div>
                </div>
              )}
            </div>
                           
            <Updates />
          </div>
      );
    }
}

function mapStateToProps(state){
  const {questionFetching,question} = state.questions;
  const { currentUserData} = state.users

  return{
    questionFetching,
    question,
    currentUserData
  }
}

export default connect(mapStateToProps,{getQuestion,getQuestions})(Question);