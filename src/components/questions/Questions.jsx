import React from "react"
import {connect} from "react-redux"
import {getQuestions} from "../../actions/questionsAction"
import { getCurrentAuth} from "../../actions/userAction"
import {Link} from "react-router-dom";
import Updates from "../latest/Updates";

class Questions extends React.Component{
  constructor(props){
    super(props);
    this.state = {
      userData: localStorage.getItem('userData') || null
    }
  }

  componentDidMount(){
    this.props.getQuestions()
    this.props.getCurrentAuth()
  }

    render(){
      const { listOfQuestions, questionFetching, currentUserData} = this.props
    
      return(
        <div className="container-fluid">
          {questionFetching && <div className="loading"></div>}
          <h3 className="display3 mt-3">Latest Queries</h3>
          <div className="row mx-0">
                <div className="col-sm-9 col-md-9 col-lg-9 px-0">
                {listOfQuestions && listOfQuestions.map((list,index)=>
                  <Link to={"/questions/" + list._id} className="noDecoration" key={index}>
                    <div className="border rounded p-2 my-3">
                      <p className="question">
                        {list.title}
                      </p>
                      <div className="row mx-0">
                        <div className="col-sm-6  col-md-6 col-lg-6 px-0">
                            <p className="mb-1"><span className="text-primary p-1">Tags: </span>
                            {list.tags &&
                              list.tags.map((tag,index)=>
                              <span className="text-secondary questionTags" key={index}>{tag};</span>
                            )}
                            </p>
                        </div>

                      </div>

                      <span><i className="fa fa-eye"></i> {list.views} views</span>
                      { list.status===1 ?
                        <span className="float-right mx-2 text-success"><i className="fa fa-smile-o" aria-hidden="true"></i> Closed </span>
                      :  <span className="float-right mx-2 text-danger"><i className="fa fa-frown-o" aria-hidden="true"></i> Open </span>
                      }
                      <span className="float-right mx-2"><i className="fa fa-check"></i> {list.answers && list.answers.length} answers</span>
                    </div>
                  </Link>
                )
                }
                </div>
                    
                  <Updates />
          </div>
        </div>
      );
    }
}

function mapStateToProps(state){
  const {listOfQuestions,questionFetching} = state.questions
  const { currentUserData} = state.users
  return{
    listOfQuestions,
    questionFetching,
    currentUserData
  }
}

export default connect(mapStateToProps, { getQuestions, getCurrentAuth})(Questions);
