import React from "react";
import axios from "axios";
// import Editor from "./editor";
import Tagpopup from "./Tagpopup";
import { dataRef } from "../../config/Firebase";

class PostQuestion extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            description: "",
            query: "",
            title: "",
            project: "",
            technology: "",
            phone: "",
            formNumber: "",
            isEmpDisplay: false,
            isTechDisplay: false,
            employees: require("../../utils/employees.json"),
            technologies: require("../../utils/technologies.json"),
            searchValue: '',
            searchTechValue: '',
            items: [],
            taggedEmployees: [],
            unTaggedEmployees: [],
            techItems: [],
            taggedTech: [],
            unTaggedTech: [],
            mailNotify:"",
            loading : false
        }

        this.onhandleChange = this.onhandleChange.bind(this);
        this.postQueryData = this.postQueryData.bind(this);
        this.getEditordata = this.getEditordata.bind(this);
        this.tagPerson = this.tagPerson.bind(this);
        this.unTagPerson = this.unTagPerson.bind(this);
        this.search = this.search.bind(this);
    }

    tagPerson = (item, tag, untag, display) => {
        let data = this.state[untag];
        let taggedEmployees = this.state[tag];
        let currentIndex;
        data.forEach((i, index) => {
            if (i.id === item.id) {
                taggedEmployees.push(i);
                currentIndex = index;
            }
        });
        data.splice(currentIndex, 1);
        this.setState(state => {
            return {
                ...state,
                [untag]: data,
                [tag]: taggedEmployees,
                searchValue: '',
                searchTechValue: '',
                [display]: false
            }
        })
    }

    unTagPerson = (item, tag, untag) => {
        let data = this.state[tag];
        let unTaggedEmployees = this.state[untag];
        let currentIndex;
        data.forEach((i, index) => {
            if (i.id === item.id) {
                unTaggedEmployees.push(i);
                currentIndex = index;
            }
        });
        data.splice(currentIndex, 1);
        this.setState(state => {
            return {
                ...state,
                [untag]: unTaggedEmployees,
                [tag]: data
            }
        })
    }

    onhandleChange = (event) => {
        this.setState({
            [event.target.name]: event.target.value
        })
    }

    getEditordata = (data) => {
        this.setState({
            query: data
        });
    }

    // togglePopup = () => {
    //     this.setState({
    //         isopen: !isopen
    //     })
    // }

    postQueryData = (event) => {
        event.preventDefault();
        this.setState({
          loading:true
        })
        let id = new Date();
        id = id.toLocaleString();
        id = id.replace(/[/,: ]/g, "");

        let currentdate = new Date();
        let created_at = currentdate.toISOString();

        let data = {
            answers: [
                {
                    "id": 0,
                    "name": "Wilder Holt",
                    "likes":0,
                    "code":"asdf",
                    "helped":true,
                    "solved":false,
                    "answer": "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Porro deleniti temporibus, ratione exercitationem voluptas eaque quos debitis eligendi, illum, doloremque laudantium corrupti? Expedita tempore aspernatur beatae dolore nobis vero praesentium."
                  }
            ],
            _id: id,
            title: this.state.title,
            question: this.state.description,
            code: this.state.query,
            created_at: created_at,
            project: this.state.project,
            tags: this.state.taggedTech.map(t => t.name),
            status: 0,
            tagged_people: this.state.taggedEmployees,
            views: 0
        }
        dataRef.push().set(data);
        // addQuery(data);
        //this.props.history.push("/dashboard");

        // var templateParams={
        //   from_name : "Test_name", //get name from auth username
        //   to_name : 'karthikkumar3k@gmail.com',
        //   to_email: ['karthikkumar3k@gmail.com', 'dpvs6345@gmail.com'],
        //   subject:"Lorem ipsum dolor sit amet",
        //   message_html : "<a href='http://localhost:3000/questions/"+ id +"'>" + this.state.title + "</a>"
        // }
        // emailjs.send('smtp_server','template_tYdipGCz', templateParams,'user_ggAbITAJeIvgBXlNB4b37')
        // .then(function(response) {
        //    console.log('SUCCESS!', response.status, response.text);
        // }, function(err) {
        //    console.log('FAILED...', err);
        // });
        axios.post('http://127.0.0.1:8000/mail',data)
        .then(res=>this.setState({
          mailNotify:"mail is been sent successfully",
          loading : false
        }),function(){
          this.props.history.push('/question/'+ id)
        })
        .catch(err=>this.setState({
          mailNotify : "something went wrong",
          loading : false
        }))

    }

    componentDidMount = () => {
        this.setState(state => {
            return {
                ...state,
                unTaggedEmployees: state.employees,
                unTaggedTech: state.technologies
            }
        })
    }

    search = (event, untag, display, search, item) => {
        let value = event.target.value;
        let data = this.state[untag];
        let items = [];
        data.forEach((d) => {
            if (d.name.toLowerCase().indexOf(value.toLowerCase()) > -1) {
                items.push(d);
            }
        })
        if (value === "") {
            items = []
        }

        this.setState(state => {
            return {
                ...state,
                [item]: items,
                [search]: value,
                [display]: items.length > 0 ? true : false
            }
        })
    }

    render() {
        const { items, isTechDisplay, isEmpDisplay, techItems, mailNotify, loading } = this.state;
        return (
            <div className="container my-3">
              <div className="row">
              <div className="col-md-10  offset-md-1"><h4 className="text-center pb-2">Post Query</h4></div>
                 <div className="col-md-10  offset-md-1 bgformcolor">
                 <form onSubmit={this.postQueryData} id="myForm">
                        
                        {mailNotify && <div className="alert alert-primary">
                        {mailNotify}</div>}
                        {loading && <div className="loading"></div>}
                        <div className="form-group">
                            <input type="text" placeholder="Enter the query title" name="title" className="form-control"
                                value={this.state.title}
                                onChange={this.onhandleChange} />
                        </div>
                        <div className="form-group">
                            <textarea cols="80" rows="2"
                                name="description"
                                value={this.state.description} className="form-control"
                                onChange={this.onhandleChange}
                                placeholder="Write description here ............."></textarea>
                        </div>
                        <div className="form-group">
                            <textarea cols="80" rows="4"
                                name="query"
                                value={this.state.query} className="form-control"
                                onChange={this.onhandleChange}
                                placeholder="Write code here ............."></textarea>
                        </div>
                        {/* <Editor className="editor"
                            content={this.state.query}
                            change={this.getEditordata}
                        /> */}
                        <div className="form-group">
                                <label className="chooseOption"> Select Project </label>
                            <select name="project"
                                value={this.state.project} className="form-control"
                                onChange={this.onhandleChange}
                            >
                                <option value="Select Project">Select Project</option>
                                <option value="Project1">Project 1</option>
                                <option value="project2">Project 2</option>
                                <option value="project3">Project 3</option>
                                <option value="project4">Project 4</option>
                                <option value="project5">Project 5</option>
                            </select>
                        </div>
                        <div className="form-group">
                            <input type="text" placeholder="Search Technology for tagging"
                            className="form-control"
                            onChange={(e) => this.search(e, "unTaggedTech", "isTechDisplay", "searchTechValue", "techItems")} value={this.state.searchTechValue} />
                            {techItems.length > 0 && isTechDisplay ? <div className="popup_body">
                                {techItems.map((item, index) => {
                                    return (
                                        <div key={index} className="tag_inner" onClick={this.tagPerson.bind(this, item, "taggedTech", "unTaggedTech", "isTechDisplay")}>{item.name}</div>
                                    )
                                })}
                            </div> : null}
                            <div className="pt-1 text-secondary">Tag Technology : {
                                this.state.taggedTech.map((emp, index) => {
                                    return (
                                        <span key={index} className="tag text-success p-1 m-1">{emp.name} <i className="fa fa-times-circle-o" onClick={this.unTagPerson.bind(this, emp, "taggedTech", "unTaggedTech")} aria-hidden="true"></i></span>
                                    )
                                })
                            }</div>
                        </div>
                        <div className="form-group">
                            <input type="text" placeholder="Search employee for tagging"
                            className="form-control"
                             onChange={(e) => this.search(e, "unTaggedEmployees", "isEmpDisplay", "searchValue", "items")} value={this.state.searchValue} />
                            {items.length > 0 && isEmpDisplay ? <div className="popup_body">
                                {items.map((item, index) => {
                                    return (
                                        <div key={index} className="tag_inner" onClick={this.tagPerson.bind(this, item, "taggedEmployees", "unTaggedEmployees", "isEmpDisplay")}>{item.name}</div>
                                    )
                                })}
                            </div> : null}
                            <div className="pt-1 text-secondary">Tag Peoples : {
                                this.state.taggedEmployees.map((emp, index) => {
                                    return (
                                        <span key={index} className="tag text-success p-1 m-1">{emp.name} <i className="fa fa-times-circle-o" onClick={this.unTagPerson.bind(this, emp, "taggedEmployees", "unTaggedEmployees")} aria-hidden="true"></i></span>
                                    )
                                })
                            }</div>
                        </div>
                        {/*
                          <div className="form-group">
                            <input type="file" className="form-control" />
                          </div>
                          */}

                        <div className="form-group mb-3">
                            <button type="submit" className="btn btn-success btn-sm float-right">Submit</button>
                            <button type="reset" className="btn btn-secondary btn-sm float-right mx-2">Reset</button>
                        </div>
                    </form>
                 
                 </div>
              
              </div>
                  
                {this.state.isopen ? <Tagpopup toggle={this.togglePopup} tag={this.tagPerson} data={this.state.unTaggedEmployees} /> : null}
            </div>
        )
    }
}
export default PostQuestion
//
// const mapStateToProps = (state) => {
//     return {
//         data: state.initialData
//     }
// }
//
// const mapDispatchToProps = (dispatch) => {
//     return {
//         getData: () => dispatch(getQuery("GET_QUERY_DATA"))
//     }
// }
//
// export default connect(mapStateToProps, mapDispatchToProps)(PostQuestion);
