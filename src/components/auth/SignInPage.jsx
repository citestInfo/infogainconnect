import React from "react"
import SignInForm from "./SignInForm"
import FirebaseContext from '../../config/FirebaseContext';

class SignInPage extends React.Component {
    render() {
        return (
            <FirebaseContext.Consumer>
                {firebase => 
                <React.Fragment>
                    <SignInForm firebase={firebase} />
                </React.Fragment>
                }
            </FirebaseContext.Consumer>
        );
    }
}
export default SignInPage;
