import React from "react"
import PasswordChange from "./PasswordChange"
import { Link } from "react-router-dom"
import FirebaseContext from '../../config/FirebaseContext';

class PasswordChangePage extends React.Component {
    render() {
        return (
            <FirebaseContext.Consumer>
                {firebase =>
                    <React.Fragment>
                        <PasswordChange firebase={firebase} />
                        <Link to="/signin" className="pull-right">Sign in</Link>
                    </React.Fragment>
                }
            </FirebaseContext.Consumer>
        );
    }
}
export default PasswordChangePage;
