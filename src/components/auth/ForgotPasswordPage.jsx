import React from "react"
import ForgotPassword from "./ForgotPassword"
import FirebaseContext from '../../config/FirebaseContext';

class ForgotPasswordPage extends React.Component {
    render() {
        return (
            <FirebaseContext.Consumer>
                {firebase => <ForgotPassword firebase={firebase} />}
            </FirebaseContext.Consumer>
        );
    }
}
export default ForgotPasswordPage;
