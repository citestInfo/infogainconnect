import React, { Component } from 'react';
import { withRouter, Link } from "react-router-dom"

class SignInForm extends Component {
    constructor(props) {
        super(props);

        this.state = {
            email: '',
            password: '',
            role: '',
            error: null,
            isLogin: false,
            isLoading: false
        };
    }

    onSubmit = event => {
        event.preventDefault();
        const { email, password } = this.state;
        const that = this;
        this.setState({
            isLoading: true
        }, function () {
            that.props.firebase
                .doSignInWithEmailAndPassword(email, password)
                .then(() => {
                    that.setState((state, props) => ({
                        ...state,
                        isLogin: !state.isLogin,
                        isLoading: !state.isLoading
                    }), function () {
                        that.props.history.push("/");
                    }
                    );
                })
                .catch(error => {
                    that.setState({ error, isLogin: false, isLoading: false });
                });
            if (that.state.isLogin) {
                that.props.history.push("/");
            }

        });

    };

    onChange = event => {
        this.setState({ [event.target.name]: event.target.value });
    };

    render() {
        const { email, password, error } = this.state;

        const isInvalid = password === '' || email === '';
        
        
        return (
            <div className="container">
                <div className="row">
                    <div className="col-md-8 offset-md-2 signinbox">
                        <div className="col-md-6 imgdiv">
                        </div>
                        <div className="col-md-6 p-0">
                            <div className="text-center mt-2" >
                                <span className="headerlogo">
                                    <img src="/img/logo.png" alt="igconnect" />
                                </span>
                            </div>
                            <form onSubmit={this.onSubmit} className="loginformstyle">
                                <div className="m-4">
                                    {error && <p className="loginerrmsg">{error.message}</p>}
                                    <div className="form-group">
                                        <input
                                            name="email"
                                            value={email}
                                            onChange={this.onChange}
                                            type="text"
                                            placeholder="Email Address"
                                            className="form-control"
                                        />
                                    </div>
                                    <div className="form-group">
                                        <input
                                            name="password"
                                            value={password}
                                            onChange={this.onChange}
                                            type="password"
                                            placeholder="Password"
                                            className="form-control"
                                        />
                                    </div>
                                    <div className="mb-3">
                                        <Link to="/forgot-password">Forgot Password?</Link>
                                        <button className="btn btn-sm btn-success float-right " disabled={isInvalid} type="submit">Sign In</button>
                                        <button className="btn btn-sm btn-dark float-right mx-2" type="reset">Reset</button>
                                    </div>
                                    <Link to="/signup" className="noDecoration">Click here to <span className="text-info">Sign Up</span></Link>
                                </div>
                                
                            </form>
                        </div>
                    </div>
                </div>
            </div>
  
        );
    }
}


export default withRouter(SignInForm)