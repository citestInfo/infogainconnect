import React from "react"
import SignupForm from "./SignupForm"
import FirebaseContext from '../../config/FirebaseContext';

class SignupPage extends React.Component{
    render(){
        return(
            <FirebaseContext.Consumer>
                {firebase => <SignupForm firebase={firebase} />}
            </FirebaseContext.Consumer>
        );
    }
}
export default SignupPage;
