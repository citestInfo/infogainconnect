import React, { Component } from 'react';
import { Link} from "react-router-dom"

const INITIAL_STATE = {
    email: '',
    error: null,
};

class ForgotPassword extends Component {
    constructor(props) {
        super(props);
        this.state = { ...INITIAL_STATE };
    }

    onSubmit = event => {
        const { email } = this.state;

        this.props.firebase
            .doPasswordReset(email)
            .then(() => {
                this.setState({ ...INITIAL_STATE });
            })
            .catch(error => {
                this.setState({ error });
            });

        event.preventDefault();
    };

    onChange = event => {
        this.setState({ [event.target.name]: event.target.value });
    };

    render() {
        const { email, error } = this.state;

        const isInvalid = email === '';

        return (
            <div className="container">
                <h4 className="text-center my-4">Forgot Password</h4>
                <form onSubmit={this.onSubmit}>
                    <div className="row mx-0">
                        <div className="col-sm-4 offset-sm-4 col-md-4 offset-md-4 col-lg-4 offset-lg-4">
                            {error && <p>{error.message}</p>}
                            <div className="form-group">
                                <input
                                    name="email"
                                    value={this.state.email}
                                    onChange={this.onChange}
                                    type="text"
                                    placeholder="Email Address"
                                    className="form-control"
                                />
                            </div>
                            <Link to="/signin">Sign in</Link>
                            <button disabled={isInvalid} type="submit" className="btn btn-success btn-sm pull-right">
                                        Reset My Password
                            </button>

                        </div>
                    </div>
                </form>
            </div>
        );
    }
}


export default ForgotPassword;
