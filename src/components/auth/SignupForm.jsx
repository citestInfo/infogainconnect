import React from "react";
import {Link} from "react-router-dom";
import firebase from "firebase";
import {connect} from "react-redux";
import {userDetailsPost} from "../../actions/userAction";

class SignupForm extends React.Component{
    state={
        username : "",  
        email: "",
        phone: "",
        password:"",
        confirm_password: "",
        role: "",
        error:null
    }

    handleSubmit = (e) => {
        e.preventDefault();
        const { email, password, role, username, phone } = this.state;
        
        this.props.firebase.doCreateUserWithEmailAndPassword(email, password)
            .then(authUser => {
                const data = {
                    "uid": firebase.auth().currentUser.uid,
                    "name":username,
                    "email":email,
                    "phoneNumber":phone,
                    "role" : role
                }
                this.props.userDetailsPost(data)
                this.setState({ ...this.state });
                this.props.history.push("/");   
                
                }, err => {
                
            })
            .catch(error => {
                this.setState({ error:error });
            });
    }

    handleInput = (e) => {
        const name = e.target.name;
        const value = e.target.value;
            this.setState({
                [name] : value
            });
    }

    render(){
        const { username, email, phone, password, confirm_password,error, role} = this.state
        const isInvalid = password !== confirm_password || 
                          password === '' || email === '' || username === '';
        return (
            <div className="container">
                <div className="row">
                    <div className="col-md-8 offset-md-2 signupbox">
                            <div className="col-md-6 imgdiv">
                            </div>
                            <div className="col-md-6 p-0">
                                <div className="text-center mt-2" >
                                    <span className="headerlogo"><img src="/img/logo.png" alt="igconnect" /></span>
                                </div>
                                <form onSubmit={this.handleSubmit} className="signform">
                                {error && <p className="loginerrmsg">{error.message}</p>}
                                    <div className="form-group">
                                        <input type="text" className="form-control"
                                            name="username"  placeholder="Enter your user name"
                                            onChange={this.handleInput}
                                            value={username}
                                        />
                                    </div>
                                    <div className="form-group">
                                        <input type="email" className="form-control"
                                            name="email"  placeholder="Enter your email"
                                            onChange={this.handleInput}
                                            value={email}
                                        />
                                    </div>
                                    <div className="form-group ">
                                        <input type="text" className="form-control"
                                            name="phone"  placeholder="Enter your phone"
                                            onChange={this.handleInput}
                                            value={phone}
                                        />
                                    </div>
                                    <div className="form-group">
                                        <input type="password" className="form-control"
                                            name="password"  placeholder="Enter your password"
                                            onChange={this.handleInput}
                                            value={password}
                                        />
                                    </div>
                                    <div className="form-group">
                                        <input type="password" className="form-control"
                                                name="confirm_password"  placeholder="Enter confirm password "
                                            onChange={this.handleInput}
                                            value={confirm_password}
                                        />
                                    </div>
                                    <div className="form-group">
                                        <select name="role" 
                                            className="form-control"
                                            onChange={this.handleInput} value={role}>
                                            <option value="null">Select your role</option>
                                            <option value="tech">Tech</option>
                                            <option value="hradmin">HR Admin</option>
                                            <option value="job">HR Job</option>
                                            <option value="training">HR Training</option>
                                            <option value="facility">Facility/Admin</option>
                                            <option value="facility">Tech Support</option>
                                        </select>
                                    </div>
                                    <div className="col-12">
                                        <button className="btn btn-sm btn-success float-right mb-2" disabled={isInvalid} type="submit">Submit</button>
                                        <button className="btn btn-sm btn-dark float-right mx-2 mb-2" type="reset">Reset</button>
                                    </div>
                                <Link to="/signin" className="noDecoration">Click here to <span className="text-info">Sign in</span></Link>
                                </form>
                            </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default connect(null, {userDetailsPost})(SignupForm);