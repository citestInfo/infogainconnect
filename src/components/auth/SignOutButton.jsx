import React from "react"
import FirebaseContext from '../../config/FirebaseContext';

class SignOutButton extends React.Component {
    render() {
        return (
            <FirebaseContext.Consumer>
                {firebase => <button type="button" onClick={firebase.doSignOut} className="signoutbtn float-right mx-2"
                >
                    <i className="fa fa-sign-out" style={{ 'color': '#fff' }}></i> Logout
                    </button>
                
            }
            </FirebaseContext.Consumer>
        );
    }
}
export default SignOutButton;
