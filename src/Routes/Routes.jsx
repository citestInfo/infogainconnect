import React from 'react'
import { Route, Switch, Redirect} from 'react-router-dom';
import Questions from '../components/questions/Questions';
import Question from '../components/questions/Question';
import Latest from '../components/latest/Latest';
import Blog from '../components/latest/Blog';
import SaveJobs from '../components/latest/SaveJobs';
import AddMore from '../components/latest/AddMore';
import Jobs from '../components/jobs/Jobs';
import AllUpdates from '../components/latest/AllUpdates';
import Dashboard from '../components/dashboard/Dashboard';
import PostTraining from '../components/latest/PostTraining';
import AddMorePost from '../components/latest/AddMorePost';
import PostQuestion from '../components/questions/PostQuestion';
import EditQuestion from '../components/questions/EditQuestion';
import HRPosts from '../components/posts/HRPosts';
import ISSPosts from '../components/posts/ISSPosts';
import NotFound from '../components/NotFound';
import Main from '../components/Main';
import SignupPage from '../components/auth/SignupPage';
import SignInPage from '../components/auth/SignInPage';
import PasswordChangePage from '../components/auth/PasswordChangePage';
import ForgotPasswordPage from '../components/auth/ForgotPasswordPage';

const Routes = ({props,auth}) => (
         <div>
              <Switch>
                 <Route  path="/signin" component={SignInPage} />
                  <Route  path="/signup" component={SignupPage} />
                  <Route  path="/forgot-password" component={ForgotPasswordPage} />
                <Main>
                  {auth ? 
                    <React.Fragment>
                      <Route exact path="/" component={Questions} />
                      <Route exact path="/questions/:id?" component={Question} />
                      <Route exact path="/questions/edit/:id?" component={EditQuestion} />
                      <Route exact path="/latest/:id?" component={Latest} />
                      <Route exact path="/latest/blogs/:author?" component={Blog} />
                      <Route exact path="/jobs" component={Jobs} />
                      <Route exact path="/trainings" component={AllUpdates} />
                      <Route exact path="/hradmin" component={AllUpdates} />
                      <Route exact path="/issadmin" component={AllUpdates} />
                      <Route exact path="/post/job" component={SaveJobs} />
                      <Route exact path="/training" component={PostTraining} />
                      <Route exact path="/hr" component={HRPosts} />
                      <Route exact path="/iss" component={ISSPosts} />
                      <Route exact path="/query/ask" component={PostQuestion} />
                      <Route exact path="/addmore/:type?" component={AddMore} />
                      <Route exact path="/latest/addmore/:id?" component={AddMorePost} /> 
                      <Route exact path="/change-password" component={PasswordChangePage} />
                      <Route exact path="/dashboard" component={Dashboard} />
                    </React.Fragment>
                    :
                    <Redirect to='/signin' />
                  }
                 
                </Main>
                  <Route exact path="*" component={NotFound}/>
              </Switch>
        </div>
);

export default Routes;
