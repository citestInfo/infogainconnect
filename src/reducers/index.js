import { combineReducers } from 'redux';
import questionReducer from './questionReducer';
import latestReducer from './latestReducer';
import jobReducer from './jobReducer';
import trainingReducer from './trainingReducer';
import hrPostsReducer from './hrPostsReducer';
import ISSPostsReducer from './ISSPostsReducer';
import userReducer from './userReducer';
import postsReducer from './postsReducer';
import getData from './getData';

const rootReducer = combineReducers({
  jobs : jobReducer,
  questions : questionReducer,
  latestUpdates : latestReducer,
  training : trainingReducer,
  hrPosts : hrPostsReducer,
  issPosts : ISSPostsReducer,
  addMorePosts: postsReducer,
  users: userReducer,

  // manikonda reducer
  getData : getData
});

// const rootReducer = (state, action) => {
//
//   return appReducer(state, action)
// }
export default rootReducer
