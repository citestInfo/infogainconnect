import {
    POSTS_PROCESSING,
    FETCHED_POSTS_SUCCESSFULLY,
    FETCH_POSTS_FAILED,
    POSTED_POST_SUCCESSFULLY,
    POST_FAILED
} from '../actions/ActionTypes';

const initialState = {
    listOfPosts: [],
    post: [],
    postFetching: false,
    postSuccess: null,
    postError : ""
};

const postsReducer = (state = initialState, action = {}) => {
    switch (action.type) {
        case POSTS_PROCESSING:
            return {
                ...state,
                postFetching: true,
                listOfPosts: [],
                postSuccess : "",
                postError: null
            };
        case FETCHED_POSTS_SUCCESSFULLY:
            return {
                ...state,
                postFetching: false,
                listOfPosts: action.payload,
                postFetchError: null,
            };
        case FETCH_POSTS_FAILED:
            return {
                ...state,
                postFetching: false,
                listOfPosts: [],
                postFetchError: action.payload
            };
        case POSTED_POST_SUCCESSFULLY:
            return {
                ...state,
                postFetching: false,
                post: action.payload,
                postError: ""
            };
        case POST_FAILED:
            return {
                ...state,
                postFetching: false,
                post: [],
                postError: action.payload
            };
        default:
            return state;
    }
}

export default postsReducer
