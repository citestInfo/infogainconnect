import {
     USER_PROCESSING,
     POSTED_USER_DETAILS_SUCCESSFULLY,
     POST_USER_DETAILS_FAILED,
     FETCHED_CURRENT_USER_DETAILS_SUCCESSFULLY,
     FETCH_CURRENT_USER_DETAILS_FAILED,
     UPDATED_USER_DATA_SUCCESSFULLY,
     UPDATED_USER_DATA_FAILED
} from '../actions/ActionTypes';

const initialState = {
    userProcessing: false,
    currentUser: [],
    currentUserError: "",
    postedUserDetailsSuccess : "",
    postedUserDetailsFailed : "",
    currentUsetFetchError:"",
    currentUserData:{},
    currentUserDataError:""
};

const userReducer = (state = initialState, action = {}) => {
    switch (action.type) {
        case USER_PROCESSING:
            return {
                ...state,
                userProcessing: true,
                postedUserDetailsSuccess:"",
                postedUserDetailsFailed:""
            };
        case POSTED_USER_DETAILS_SUCCESSFULLY:
            return {
                ...state,
                userProcessing: false,
               postedUserDetailsSuccess: "Posted Successfully",
               postedUserDetailsFailed : ""
            };
        case POST_USER_DETAILS_FAILED:
            return {
                 ...state,
                 userProcessing: false,
                postedUserDetailsSuccess: "",
                postedUserDetailsFailed : "Invalid Information"
            };
        case FETCHED_CURRENT_USER_DETAILS_SUCCESSFULLY:
            return {
                ...state,
                userProcessing: false,
                currentUser : action.payload,
                currentUsetFetchError : ""
            };
        case FETCH_CURRENT_USER_DETAILS_FAILED:
            return {
                ...state,
                userProcessing: false,
                currentUser: [],
                currentUserFetchError : "Could not fetch the requested user"
            };
        case UPDATED_USER_DATA_SUCCESSFULLY:
            return {
                ...state,
                userProcessing: false,
                currentUserData : action.payload,
                currentUserDataError : ""
            };
        case UPDATED_USER_DATA_FAILED:
            return {
                ...state,
                userProcessing: false,
                currentUserData: {},
                currentUserDataError : "Could not update the requested user details"

            };
        default:
            return state;
    }
}

export default userReducer
