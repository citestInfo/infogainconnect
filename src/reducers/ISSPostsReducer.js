import {
          ISS_POSTS_PROCESSING,
          FETCHED_ISS_POSTS_SUCCESSFULLY,
          FETCH_ISS_POSTS_FAILED,
          POSTED_ISS_POSTS_SUCCESSFULLY,
          POST_ISS_POSTS_FAILED,
          FETCHED_ISS_POST_SUCCESSFULLY,
          FETCH_ISS_POST_FAILED
      } from '../actions/ActionTypes';

      const initialState = {
        issPostsProcessing : false,
        listOfIssPosts : [],
        listOfIssPostsError : "",
        listOfIssPostsSuccess : "",
        IssPost:[],
        IssPostError:"",
        IssPostSuccess:""
      };

      const ISSPostsReducer = (state = initialState, action = {}) => {
        switch(action.type) {
          case ISS_POSTS_PROCESSING:
              return {
                ...state,
                issPostsProcessing : true,
                listOfIssPostsError : null
              };
          case POSTED_ISS_POSTS_SUCCESSFULLY:
              return {
                ...state,
                issPostsProcessing : false,
                listOfIssPosts : action.payload,
                listOfIssPostsError : null,
                listOfIssPostsSuccess : "Posted Successfully"
              };
          case POST_ISS_POSTS_FAILED:
              return {
                ...state,
                issPostsProcessing : false,
                listOfIssPosts : [],
                listOfIssPostsError : action.payload,
                listOfIssPostsSuccess : ""
              };
          case FETCHED_ISS_POSTS_SUCCESSFULLY:
              return {
                ...state,
                issPostsProcessing : false,
                listOfIssPosts : action.payload,
                listOfIssPostsError : null,
                listOfIssPostsSuccess : "Posted Successfully"
              };
          case FETCH_ISS_POSTS_FAILED:
              return {
                ...state,
                issPostsProcessing : false,
                listOfIssPosts : [],
                listOfIssPostsError : action.payload,
                listOfIssPostsSuccess : ""
              };
          case FETCHED_ISS_POST_SUCCESSFULLY:
              return {
                ...state,
                issPostsProcessing : false,
                IssPost : action.payload,
                IssPostError : null,
                IssPostSuccess : "Posted Successfully"
              };
          case FETCH_ISS_POST_FAILED:
              return {
                ...state,
                issPostsProcessing : false,
                IssPost : [],
                IssPostError : action.payload,
                IssPostSuccess : ""
              };
              default :return state;
      }
  }

  export default ISSPostsReducer
