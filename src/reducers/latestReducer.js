import {LATESTUPDATE_PROCESSING,
        FETCHED_ALL_LATESTUPDATES_SUCCESSFULLY,
        FETCH_ALL_LATESTUPDATES_FAILED,
        FETCH_LATESTUPDATE_SUCCESSFULLY,
        FETCH_LATESTUPDATE_FAILED,
        LATEST_TECH_TRENDS,
        LATEST_TECH_TRENDS_FAILED,
        FETCH_LATEST_TECH_UPDATE_SUCCESSFULLY,
        FETCH_LATEST_TECH_UPDATE_FAILED
      } from '../actions/ActionTypes';

      const initialState = {
        listOfLatestUpdates : [],
        latestUpdate : [],
        latestTechTrends:[],
        tech:[],
        latestUpdateFetching : false,
        latestUpdateFetchError : null,
      };

      const latestReducer = (state = initialState, action = {}) => {
        switch(action.type) {
          case LATESTUPDATE_PROCESSING:
              return {
                ...state,
                latestUpdateFetching : true,
                latestUpdateFetchError : null
              };
          case FETCHED_ALL_LATESTUPDATES_SUCCESSFULLY:
              return {
                ...state,
                latestUpdateFetching : false,
                listOfLatestUpdates : action.payload[0],
                latestUpdateFetchError : null
              };
          case FETCH_ALL_LATESTUPDATES_FAILED:
              return {
                ...state,
                latestUpdateFetching : false,
                listOfLatestUpdates : [],
                latestUpdateFetchError : action.payload
              };
          case FETCH_LATESTUPDATE_SUCCESSFULLY:
              return {
                ...state,
                latestUpdateFetching : false,
                latestUpdate : action.payload,
                latestUpdateFetchError : null
              };
          case FETCH_LATESTUPDATE_FAILED:
              return {
                ...state,
                latestUpdateFetching : false,
                latestUpdate : [],
                latestUpdateFetchError : action.payload
              };
          case LATEST_TECH_TRENDS:
              return {
                ...state,
                latestUpdateFetching : false,
                latestTechTrends : action.payload.map((tech,i)=>{tech.id = new Date().toString();return tech;}),
                latestUpdateFetchError : null
              };
          case LATEST_TECH_TRENDS_FAILED:
              return {
                ...state,
                latestUpdateFetching : false,
                latestTechTrends : [],
                latestUpdateFetchError : action.payload
              };
          case FETCH_LATEST_TECH_UPDATE_SUCCESSFULLY:
              return {
                ...state,
                latestUpdateFetching : false,
                tech : action.payload,
                latestUpdateFetchError : null
              };
          case FETCH_LATEST_TECH_UPDATE_FAILED:
              return {
                ...state,
                latestUpdateFetching : false,
                tech : [],
                latestUpdateFetchError : action.payload
              };
              default:return state;
          }
      }

export default latestReducer
