import {HR_POSTS_PROCESSING,
          FETCHED_HR_POSTS_SUCCESSFULLY,
          FETCH_HR_POSTS_FAILED,
          POSTED_HR_POSTS_SUCCESSFULLY,
          POST_HR_POSTS_FAILED,
          FETCHED_HR_POST_SUCCESSFULLY,
          FETCH_HR_POST_FAILED,
      } from '../actions/ActionTypes';

      const initialState = {
        hrPostsProcessing : false,
        listOfHRPosts : [],
        listOfHRPostsError : "",
        listOfHRPostsFetchError: "",
        listOfHRPostsSuccess : "",
        listOfHRPostsFetchSuccess: "",
        HRPost:[],
        HRPostError:"",
        HRPostSuccess:"",
      };

      const hrPostsReducer = (state = initialState, action = {}) => {
        switch(action.type) {
          case HR_POSTS_PROCESSING:
              return {
                ...state,
                hrPostsProcessing : true,
                listOfHRPostsSuccess : "",
                listOfHRPostsError : null,
              };
          case POSTED_HR_POSTS_SUCCESSFULLY:
              return {
                ...state,
                hrPostsProcessing : false,
                listOfHRPosts : action.payload,
                listOfHRPostsError : null,
                listOfHRPostsSuccess : "Posted Successfully"
              };
          case POST_HR_POSTS_FAILED:
              return {
                ...state,
                hrPostsProcessing : false,
                listOfHRPosts : [],
                listOfHRPostsError : action.payload,
                listOfHRPostsSuccess : ""
              };
          case FETCHED_HR_POSTS_SUCCESSFULLY:
              return {
                ...state,
                hrPostsProcessing : false,
                listOfHRPosts : action.payload,
                listOfHRPostsError : null,
                listOfHRPostsFetchSuccess : "Fetched Successfully"
              };
          case FETCH_HR_POSTS_FAILED:
              return {
                ...state,
                hrPostsProcessinghrPostsProcessing : false,
                listOfHRPosts : [],
                listOfHRPostsError : action.payload,
                listOfHRPostsSuccess : ""
              };
          case FETCHED_HR_POST_SUCCESSFULLY:
              return {
                ...state,
                hrPostsProcessing : false,
                HRPost : action.payload,
                HRPostError : null,
                HRPostSuccess : "Fetched Successfully"
              };
          case FETCH_HR_POST_FAILED:
              return {
                ...state,
                hrPostsProcessing : false,
                HRPost : [],
                HRPostError : action.payload,
                HRPostSuccess : ""
              };
              default :return state;
      }
  }

  export default hrPostsReducer
