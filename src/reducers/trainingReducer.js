import {TRAINING_PROCESSING,
        POSTED_TRAINING_SUCCESSFULLY,
        POST_TRAINING_FAILED,
        FETCH_LATEST_TRAINING_SUCCESSFULLY,
        FETCH_LATEST_TRAINING_FAILED,
        FETCHED_TRAINING_SUCCESSFULLY,
        FETCH_TRAINING_FAILED
      } from '../actions/ActionTypes';

      const initialState = {
        trainingProcessing : false,
        listOfTrainings : [],
        listOfTrainingsError : "",
        listOfTrainingsSuccess : "",
        latestTraining:[],
        latestTrainingError:""
      };

      const trainingReducer = (state = initialState, action = {}) => {
        switch(action.type) {
          case TRAINING_PROCESSING:
              return {
                ...state,
                trainingProcessing : true,
                listOfTrainingsError : ""
              };
          case POSTED_TRAINING_SUCCESSFULLY:
              return {
                ...state,
                trainingProcessing : false,
                listOfTrainings : action.payload,
                listOfTrainingsError : "",
                listOfTrainingsSuccess : "Posted Successfully"
              };
          case POST_TRAINING_FAILED:
              return {
                ...state,
                trainingProcessing : false,
                listOfTrainings : [],
                listOfTrainingsError : action.payload,
                listOfTrainingsSuccess : ""
              };
          case FETCHED_TRAINING_SUCCESSFULLY:
              return {
                ...state,
                trainingProcessing : false,
                listOfTrainings : action.payload,
                listOfTrainingsError : "",
                listOfTrainingsSuccess : "Posted Successfully"
              };
          case FETCH_TRAINING_FAILED:
              return {
                ...state,
                trainingProcessing : false,
                listOfTrainings : [],
                listOfTrainingsError : action.payload,
                listOfTrainingsSuccess : ""
              };
          case FETCH_LATEST_TRAINING_SUCCESSFULLY:
              return {
                ...state,
                trainingProcessing : false,
                latestTraining : action.payload,
                latestTrainingError : "",
                latestTrainingSuccess : "Posted Successfully"
              };
          case FETCH_LATEST_TRAINING_FAILED:
              return {
                ...state,
                trainingProcessing : false,
                latestTraining : [],
                latestTrainingError : action.payload,
                latestTrainingSuccess : ""
              };
              default:return state;
          }
      }

  export default trainingReducer
