import {JOB_PROCESSING,
        FETCHED_JOB_SUCCESSFULLY,
        FETCH_JOB_FAILED,
        POSTED_JOB_SUCCESSFULLY,
        POST_JOB_FAILED,
        FETCHED_LATEST_JOB_SUCCESSFULLY,
        FETCH_LATEST_JOB_FAILED
      } from '../actions/ActionTypes';

      const initialState = {
        jobProcessing : false,
        listOfJobs : [],
        listOfJobsError : "",
        listOfJobsSuccess : "",
        latestJob:[],
        latestJobError:""
      };

      const jobReducer = (state = initialState, action = {}) => {
        switch(action.type) {
          case JOB_PROCESSING:
              return {
                ...state,
                jobProcessing : true,
                listOfJobsError : null
              };
          case POSTED_JOB_SUCCESSFULLY:
              return {
                ...state,
                jobProcessing : false,
                listOfJobs : action.payload,
                listOfJobsError : null,
                listOfJobsSuccess : "Posted Successfully"
              };
          case POST_JOB_FAILED:
              return {
                ...state,
                jobProcessing : false,
                listOfJobs : [],
                listOfJobsError : action.payload,
                listOfJobsSuccess : ""
              };
          case FETCHED_JOB_SUCCESSFULLY: 
              return {
                ...state,
                jobProcessing : false,
                listOfJobs : action.payload,
                listOfJobsError : null,
                listOfJobsSuccess : "Posted Successfully"
              };
          case FETCH_JOB_FAILED:
              return {
                ...state,
                jobProcessing : false,
                listOfJobs : [],
                listOfJobsError : action.payload,
                listOfJobsSuccess : ""
              };
          case FETCHED_LATEST_JOB_SUCCESSFULLY: 
              return {
                ...state,
                jobProcessing : false,
                latestJob : action.payload,
                latestJobError : null
              };
          case FETCH_LATEST_JOB_FAILED:
              return {
                ...state,
                jobProcessing : false,
                latestJob : [],
                latestJobError : action.payload
              };
              default:return state;
          }
      }

  export default jobReducer
