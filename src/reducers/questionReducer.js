import {QUESTION_PROCESSING,
        FETCHED_ALL_QUESTIONS_SUCCESSFULLY,
        FETCH_ALL_QUESTIONS_FAILED,
        FETCH_QUESTION_SUCCESSFULLY,
        FETCH_QUESTION_FAILED
      } from '../actions/ActionTypes';

      const initialState = {
        listOfQuestions : [],
        question : [],
        questionFetching : false,
        questionFetchError : null,
      };

      const questionReducer = (state = initialState, action = {}) => {
        switch(action.type) {
          case QUESTION_PROCESSING:
              return {
                ...state,
                questionFetching : true,
                listOfQuestions : [],
                questionFetchError : null
              };
          case FETCHED_ALL_QUESTIONS_SUCCESSFULLY:
              return {
                ...state,
                questionFetching : false,
                listOfQuestions : action.payload,
                questionFetchError : null
              };
          case FETCH_ALL_QUESTIONS_FAILED:
              return {
                ...state,
                questionFetching : false,
                listOfQuestions : [],
                questionFetchError : action.payload
              };
          case FETCH_QUESTION_SUCCESSFULLY:
              return {
                ...state,
                questionFetching : false,
                question : action.payload,
                questionFetchError : null
              };
          case FETCH_QUESTION_FAILED:
              return {
                ...state,
                questionFetching : false,
                question : [],
                questionFetchError : action.payload
              };
              default:return state;
          }
      }

export default questionReducer
