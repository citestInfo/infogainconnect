import {trainingRef} from "../config/Firebase"

import {
        TRAINING_PROCESSING,
        POSTED_TRAINING_SUCCESSFULLY,
        POST_TRAINING_FAILED,
        FETCH_LATEST_TRAINING_SUCCESSFULLY,
        FETCH_LATEST_TRAINING_FAILED,
        FETCHED_TRAINING_SUCCESSFULLY,
        FETCH_TRAINING_FAILED
      } from './ActionTypes';


export const postTraining = (data) => {
  return dispatch=>{
    dispatch({type:TRAINING_PROCESSING})
    var ref = trainingRef;
      ref.push().set(data)
    trainingRef.on("value", snapshot => {
      dispatch({
        type: POSTED_TRAINING_SUCCESSFULLY,
        payload:  Object.keys(snapshot.val()).map(i => snapshot.val()[i])
      });
    },err=>{
      dispatch({
        type: POST_TRAINING_FAILED,
        payload:  err.code
      });
    });
  }
}

export const displaylatestTraining = (id) => {
  return dispatch=>{
    dispatch({type:TRAINING_PROCESSING})
    trainingRef.on("value", snapshot => {
      const trainingArray = Object.keys(snapshot.val()).map(i => snapshot.val()[i]);
      dispatch({
        type: FETCH_LATEST_TRAINING_SUCCESSFULLY,
        payload:  trainingArray.filter((list,i)=>list.id == id),
      });
    },err=>{
      dispatch({
        type: FETCH_LATEST_TRAINING_FAILED,
        payload:  err.code
      });
    });
  }
}

export const getTrainings = (data) => {
  return dispatch=>{
    dispatch({type:TRAINING_PROCESSING})
    trainingRef.on("value", snapshot => {
      dispatch({
        type: FETCHED_TRAINING_SUCCESSFULLY,
        payload:  Object.keys(snapshot.val()).map(i => snapshot.val()[i])
      });
    },err=>{
      dispatch({
        type: FETCH_TRAINING_FAILED,
        payload:  err.code
      });
    });
  }
}
