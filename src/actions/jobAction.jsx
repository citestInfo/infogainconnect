import {jobsRef} from "../config/Firebase"


import {
        JOB_PROCESSING,
        FETCHED_JOB_SUCCESSFULLY,
        FETCH_JOB_FAILED,
        POSTED_JOB_SUCCESSFULLY,
        POST_JOB_FAILED,
        FETCHED_LATEST_JOB_SUCCESSFULLY,
        FETCH_LATEST_JOB_FAILED
      } from './ActionTypes';


export const postJob = (data) => {
  return dispatch=>{
    dispatch({type:JOB_PROCESSING})
    var ref = jobsRef;
      ref.push().set(data)

    jobsRef.on("value", snapshot => {
      dispatch({
        type: POSTED_JOB_SUCCESSFULLY,
        payload:  Object.keys(snapshot.val()).map(i => snapshot.val()[i])
      });
    },err=>{
      dispatch({
        type: POST_JOB_FAILED,
        payload:  err.code
      });
    });
  }
}

export const getInternalJobs = () => {
  return dispatch=>{
    dispatch({type:JOB_PROCESSING})
    // var postData=[];
    // postsRef.on("value",data=>{
    //   postData =  Object.keys(data.val()).map(i => data.val()[i]);
    // })
     jobsRef.on("value", snapshot => {
    //   let values = [...postData, ...Object.keys(snapshot.val()).map(i => snapshot.val()[i])];
    //   values.sort(function (a, b) {
    //     // Turn your strings into dates, and then subtract them
    //     // to get a value that is either negative, positive, or zero.
    //     return new Date(parseInt(b.date)) - new Date(parseInt(a.date));
    //   });
      // console.log(values);
      dispatch({
        type: FETCHED_JOB_SUCCESSFULLY,
        payload: Object.keys(snapshot.val()).map(i => snapshot.val()[i])
      });
    },err=>{
      dispatch({
        type: FETCH_JOB_FAILED,
        payload:  err.code
      });
    });
  }
}

export const displaylatestJobs = (id) => {
  return dispatch=>{
    dispatch({type:JOB_PROCESSING})
    jobsRef.on("value", snapshot => {
      const jobsArray = Object.keys(snapshot.val()).map(i => snapshot.val()[i]);
      dispatch({
        type: FETCHED_LATEST_JOB_SUCCESSFULLY,
        payload:  jobsArray.filter((list,i)=>list.id == id),
      });
    },err=>{
      dispatch({
        type: FETCH_LATEST_JOB_FAILED,
        payload:  err.code
      });
    });
  }
}
