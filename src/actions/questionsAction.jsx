import {dataRef} from "../config/Firebase"

import {
        QUESTION_PROCESSING,
        FETCHED_ALL_QUESTIONS_SUCCESSFULLY,
        FETCH_ALL_QUESTIONS_FAILED,
        FETCH_QUESTION_SUCCESSFULLY,
        FETCH_QUESTION_FAILED
      } from './ActionTypes';


export const getQuestions = () => {
  return dispatch=>{
    dispatch({type:QUESTION_PROCESSING})
    return dataRef.orderByChild('_id').on("value", snapshot => {
      let data = Object.keys(snapshot.val()).map(i => snapshot.val()[i]);
      data.sort((a,b)=>{
        return a._id < b._id ? 1 : -1;
      })
    dispatch({
      type: FETCHED_ALL_QUESTIONS_SUCCESSFULLY,
      payload:  data//Object.keys(snapshot.val()).map(i => snapshot.val()[i])
    });
  },err=>{
    dispatch({
      type: FETCH_ALL_QUESTIONS_FAILED,
      payload:  err.code
    });
  });
  }
}

export const getQuestion = (id) => {
  return dispatch=>{
    dispatch({type:QUESTION_PROCESSING})
    return dataRef.on("value", snapshot => {
      const questionsArray = Object.keys(snapshot.val()).map(i => snapshot.val()[i])
    dispatch({
      type: FETCH_QUESTION_SUCCESSFULLY,
      payload: questionsArray.filter((list,i)=>list._id === id),
    });
  },err=>{
    dispatch({
      type: FETCH_QUESTION_FAILED,
      payload:  err.code
    });
  });
  }
}

export const updateQuestion = (data,id) => {
  return dispatch=>{
    dispatch({type:QUESTION_PROCESSING})
    var ref = dataRef;
    ref.orderByChild('_id').equalTo(id).on("value", function(snapshot) {
      snapshot.forEach(function(child) {
          ref.child(child.key).update(data)
      });
    })
  }
}
