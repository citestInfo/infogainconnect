import {latestRef} from "../config/Firebase"
import axios from "axios";
import {
        LATESTUPDATE_PROCESSING,
        FETCHED_ALL_LATESTUPDATES_SUCCESSFULLY,
        FETCH_ALL_LATESTUPDATES_FAILED,
        FETCH_LATESTUPDATE_SUCCESSFULLY,
        FETCH_LATESTUPDATE_FAILED,
        LATEST_TECH_TRENDS,
        LATEST_TECH_TRENDS_FAILED,
        FETCH_LATEST_TECH_UPDATE_SUCCESSFULLY,
        FETCH_LATEST_TECH_UPDATE_FAILED
      } from './ActionTypes';


export const getLatest = () => {
  return dispatch=>{
    dispatch({type:LATESTUPDATE_PROCESSING})
    latestRef.on("value", snapshot => {
    dispatch({
      type: FETCHED_ALL_LATESTUPDATES_SUCCESSFULLY,
      payload:  Object.keys(snapshot.val()).map(i => snapshot.val()[i])
    });
  },err=>{
    dispatch({
      type: FETCH_ALL_LATESTUPDATES_FAILED,
      payload:  err.code
    });
  });
  }
}

export const displaylatest = (id) => {
  return (dispatch,getState)=>{
    dispatch({type:LATESTUPDATE_PROCESSING})
    let listUpdates = getState().latestUpdates.listOfLatestUpdates;
    if(listUpdates.length > 0){
      return  dispatch({
          type: FETCH_LATESTUPDATE_SUCCESSFULLY,
          payload: listUpdates.filter((list,index)=>list.id === id),
        });
    }else{
        return latestRef.on("value", snapshot => {
          const latestArray = Object.keys(snapshot.val()).map(i => snapshot.val()[i])
          dispatch({
            type: FETCH_LATESTUPDATE_SUCCESSFULLY,
            payload: latestArray.filter((list,i)=>list.id === id),
          });
      },err=>{
        dispatch({
          type: FETCH_LATESTUPDATE_FAILED,
          payload:  err.code
        });
      });
    }

  }
}

export const getTrendingTech = () => {
  return dispatch=>{
    return axios.get("https://newsapi.org/v2/top-headlines?sources=techcrunch&apiKey=fc568acc27bb4ce69c5f90d98f7dd3f1")
    .then(res=>{
      dispatch({
        type : LATEST_TECH_TRENDS,
        payload : res.data.articles
      })
    })
    .catch(err=>{
      dispatch({
        type: LATEST_TECH_TRENDS_FAILED,
        payload : err.response.data
      })
    })
  }
}

export const updateTech = (author) => {
  return (dispatch,getState)=>{
    dispatch({type:LATESTUPDATE_PROCESSING})
    let listTechUpdates = getState().latestUpdates.latestTechTrends;
    if(listTechUpdates.length > 0){
      return  dispatch({
          type: FETCH_LATEST_TECH_UPDATE_SUCCESSFULLY,
          payload: listTechUpdates.filter((list,index)=>list.author === author),
        });
    }else{
      return dispatch(getTrendingTech()).then(res=>{
              let listTechUpdates = getState().latestUpdates.latestTechTrends;
              return dispatch({
                type: FETCH_LATEST_TECH_UPDATE_SUCCESSFULLY,
                payload: listTechUpdates.filter((list,index)=>list.author === author),
              })
        })
        .catch(err=>{
          return dispatch({
            type: FETCH_LATEST_TECH_UPDATE_FAILED,
            payload: err.response.data,
          })
        })
    }
  }
}
