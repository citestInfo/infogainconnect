import { postsRef } from "../config/Firebase"

import {
    POSTS_PROCESSING,
    FETCHED_POSTS_SUCCESSFULLY,
    FETCH_POSTS_FAILED,
    POSTED_POST_SUCCESSFULLY,
    POST_FAILED
} from './ActionTypes';

export const submitAddmorePost = (data) => {
    return dispatch => {
        dispatch({ type: POSTS_PROCESSING })
        postsRef.push().set(data)
        postsRef.on("value", snapshot => {
            dispatch({
                type: FETCHED_POSTS_SUCCESSFULLY,
                payload: Object.keys(snapshot.val()).map(i => snapshot.val()[i])
            });
        }, err => {
            dispatch({
                type: FETCH_POSTS_FAILED,
                payload: err.code
            });
        });
    }
}

export const getAddMore = () => {
    return dispatch => {
        dispatch({ type: POSTS_PROCESSING })
        return postsRef.orderByChild('id').on("value", snapshot => {
            let data = Object.keys(snapshot.val()).map(i => snapshot.val()[i]);
            data.sort((a, b) => {
                return a._id < b._id ? 1 : -1;
            })
            dispatch({
                type: FETCHED_POSTS_SUCCESSFULLY,
                payload: data//Object.keys(snapshot.val()).map(i => snapshot.val()[i])
            });
        }, err => {
            dispatch({
                type: FETCH_POSTS_FAILED,
                payload: err.code
            });
        });
    }
}

export const displayAddMorePost = (id) => {
    return dispatch => {
        dispatch({ type: POSTS_PROCESSING })
        postsRef.on("value", snapshot => {
            const addMorePost = Object.keys(snapshot.val()).map(i => snapshot.val()[i]);
            dispatch({
                type: POSTED_POST_SUCCESSFULLY,
                payload: addMorePost.filter((list, i) => list.id == id),
            });
        }, err => {
            dispatch({
                type: POST_FAILED,
                payload: err.code
            });
        });
    }
}