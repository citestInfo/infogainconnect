import {issPostsRef} from "../config/Firebase"

import {
        ISS_POSTS_PROCESSING,
        FETCHED_ISS_POSTS_SUCCESSFULLY,
        FETCH_ISS_POSTS_FAILED,
        POSTED_ISS_POSTS_SUCCESSFULLY,
        FETCHED_ISS_POST_SUCCESSFULLY,
        FETCH_ISS_POST_FAILED,
        POST_ISS_POSTS_FAILED
      } from './ActionTypes';


export const submitISSPost = (data) => {
  return dispatch=>{
    dispatch({type:ISS_POSTS_PROCESSING})
      issPostsRef.push().set(data)
    issPostsRef.on("value", snapshot => {
      dispatch({
        type: POSTED_ISS_POSTS_SUCCESSFULLY,
        payload:  Object.keys(snapshot.val()).map(i => snapshot.val()[i])
      });
    },err=>{
      dispatch({
        type: POST_ISS_POSTS_FAILED,
        payload:  err.code
      });
    });
  }
}

export const getISSPosts = () => {
  return dispatch=>{
    dispatch({type:ISS_POSTS_PROCESSING})
    issPostsRef.on("value", snapshot => {
      dispatch({
        type: FETCHED_ISS_POSTS_SUCCESSFULLY,
        payload:  Object.keys(snapshot.val()).map(i => snapshot.val()[i])
      });
    },err=>{
      dispatch({
        type: FETCH_ISS_POSTS_FAILED,
        payload:  err.code
      });
    });
  }
}


export const displayISS = (id) => {
  return dispatch=>{
    dispatch({type:ISS_POSTS_PROCESSING})
    issPostsRef.on("value", snapshot => {
      const ISSArray = Object.keys(snapshot.val()).map(i => snapshot.val()[i]);
      dispatch({
        type: FETCHED_ISS_POST_SUCCESSFULLY,
        payload: ISSArray.filter((list,i)=>list.id == id),
      });
    },err=>{
      dispatch({
        type: FETCH_ISS_POST_FAILED,
        payload:  err.code
      });
    });
  }
}
