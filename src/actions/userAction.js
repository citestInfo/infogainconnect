import {
    usersPostsRef
} from "../config/Firebase"


import {
    USER_PROCESSING,
    UPDATED_USER_DATA_SUCCESSFULLY,
    UPDATED_USER_DATA_FAILED,
    POSTED_USER_DETAILS_SUCCESSFULLY,
    POST_USER_DETAILS_FAILED,
    FETCHED_CURRENT_USER_DETAILS_SUCCESSFULLY,
    FETCH_CURRENT_USER_DETAILS_FAILED
} from './ActionTypes';


export const userDetailsPost = (data) => {
    return dispatch => {
        dispatch({
            type: USER_PROCESSING
        })
        var ref = usersPostsRef;
        ref.push().set(data)

        usersPostsRef.on("value", snapshot => {
            dispatch({
                type: POSTED_USER_DETAILS_SUCCESSFULLY,
                payload: Object.keys(snapshot.val()).map(i => snapshot.val()[i])
            });
        }, err => {
            dispatch({
                type: POST_USER_DETAILS_FAILED,
                payload: err.code
            });
        });
    }
}

export const getCurrentUserDetails = (id) => {
    return dispatch => {
        dispatch({
            type: USER_PROCESSING
        })
        usersPostsRef.on("value", snapshot => {
            const usersArray = Object.keys(snapshot.val()).map(i => snapshot.val()[i]);
            dispatch({
                type: FETCHED_CURRENT_USER_DETAILS_SUCCESSFULLY,
                payload: usersArray.filter((list, i) => list.id == id),
            });
        }, err => {
            dispatch({
                type: FETCH_CURRENT_USER_DETAILS_FAILED,
                payload: err.code
            });
        });
    }
}

export const getCurrentAuth = () => {
    const authUser = JSON.parse(localStorage.getItem('authUser'));
    return dispatch => {
        dispatch({
            type: USER_PROCESSING
        })
        usersPostsRef.on("value", snapshot => {
            let usersArray = Object.keys(snapshot.val()).map(i => snapshot.val()[i]);
            if (authUser && authUser.uid) {
                usersArray = usersArray.filter((list, i) => {
                    if (list.uid == authUser.uid) {
                        return true;
                    }
                    return false;
                })
            }
            localStorage.setItem('userData', JSON.stringify(usersArray[0]))
            dispatch({
                type: UPDATED_USER_DATA_SUCCESSFULLY,
                payload: usersArray[0],
            });
        }, err => {
            dispatch({
                type: UPDATED_USER_DATA_FAILED,
                payload: err.code
            });
        });
    }

}