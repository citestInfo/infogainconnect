import {hrPostsRef} from "../config/Firebase"

import {
        HR_POSTS_PROCESSING,
        FETCHED_HR_POSTS_SUCCESSFULLY,
        FETCH_HR_POSTS_FAILED,
        POSTED_HR_POSTS_SUCCESSFULLY,
        POST_HR_POSTS_FAILED,
        FETCHED_HR_POST_SUCCESSFULLY,
        FETCH_HR_POST_FAILED
      } from './ActionTypes';

// Action creators
export const submitHRPost = (data) => {
  return dispatch=>{
    dispatch({type:HR_POSTS_PROCESSING})
      hrPostsRef.push().set(data)
    hrPostsRef.on("value", snapshot => {
      dispatch({
        type: POSTED_HR_POSTS_SUCCESSFULLY,
        payload:  Object.keys(snapshot.val()).map(i => snapshot.val()[i])
      });
    },err=>{
      dispatch({
        type: POST_HR_POSTS_FAILED,
        payload:  err.code
      });
    });
  }
} 

export const getHRPosts = () => {
  return dispatch=>{
    dispatch({type:HR_POSTS_PROCESSING})
    hrPostsRef.on("value", snapshot => {
      dispatch({
        type: FETCHED_HR_POSTS_SUCCESSFULLY,
        payload:  Object.keys(snapshot.val()).map(i => snapshot.val()[i])
      });
    },err=>{
      dispatch({
        type: FETCH_HR_POSTS_FAILED,
        payload:  err.code
      });
    });
  }
}

export const displayHR = (id) => {
  return dispatch=>{
    dispatch({type:HR_POSTS_PROCESSING})
    hrPostsRef.on("value", snapshot => {
      const HRArray = Object.keys(snapshot.val()).map(i => snapshot.val()[i]);
      dispatch({
        type: FETCHED_HR_POST_SUCCESSFULLY,
        payload: HRArray.filter((list,i)=>list.id == id),
      });
    },err=>{
      dispatch({
        type: FETCH_HR_POST_FAILED,
        payload:  err.code
      });
    });
  }
}

export const submitHRAddmorePost = (data) => {
 return dispatch => {
   dispatch({
     type: HR_POSTS_PROCESSING
   })
   hrPostsRef.push().set(data)
   hrPostsRef.on("value", snapshot => {
     dispatch({
       type: POSTED_HR_POSTS_SUCCESSFULLY,
       payload: Object.keys(snapshot.val()).map(i => snapshot.val()[i])
     });
   }, err => {
     dispatch({
       type: POST_HR_POSTS_FAILED,
       payload: err.code
     });
   });
 }
}
